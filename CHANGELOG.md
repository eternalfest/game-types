# 0.6.0 (2021-04-18)

- **[Breaking change]** Move Haxe extensions to the `etwin` package.

# 0.5.2 (2020-08-26)

- **[Fix]** Import `patchman.Symbol` as `ef.Symbol`.

# 0.5.1 (2020-08-25)

- **[Feature]** Add the `ef.ds` package with additional data structures to complete the ones in the Haxe standard library.
- **[Feature]** Add support for Weak collections for non-Flash platforms.
- **[Fix]** Add support for `ef.Error` on non-Flash platforms such as the Haxe interpreter.
- **[Fix]** Fix usage examples for `FrozenMap.of` and `Set.of`.
- **[Fix]** Fix enumerability of fields added by `ef.ds.Weakset.add`.

# 0.5.0 (2020-04-08)

- **[Breaking change]** Update Hammerfest identifier map
- **[Change]** Add `ef.flash.DynamicMovieClip` for clips with arbitrary properties
- **[Change]** Relax some parameters from Entity to Pos<Float>
- **[Fix]** Remove some `Dynamic`s in `hf.mode.Editor`

# 0.4.1 (2020-01-09)

- **[Fix]** Fix compilation to `js`.

# 0.4.0 (2020-01-08)

- **[Breaking change]** Import `MovieClip` and `MovieClipLoader` declarations.
- **[Fix]** Add type for `hammer_interf_item_name`.

# 0.3.2 (2020-01-06)

- **[Fix]** Remove bare `flash.` imports from `hf`.
- **[Fix]** Use dynamic class instead of `Object` typedef for flash shims.
- **[Fix]** Use flash shims during codegen.

# 0.3.1 (2019-12-28)

- **[Feature]** Add `getHaxeTypesUri` and `getObfuMapUri`.
- **[Internal]** Update dev-dependencies.

# 0.3.0 (2019-11-30)

- **[Breaking change]** Move `hf.types.*` typedefs to their relevant classes.
- **[Breaking change]** Rename `hf.types.*` typedefs to be closer to the game's own names.

# 0.2.0 (2019-11-28)

- **[Breaking change]** Introduce `hf.Hf` type to represent the game's MovieClip.

# 0.1.1 (2019-11-28)

- **[Fix]** Remove `deobfuscator.json`.

# 0.1.0 (2019-11-25)

- **[Feature]** First release.
