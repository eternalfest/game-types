import chai from "chai";
import childProcess from "child_process";
import sysPath from "path";
import url from "url";
import {
  getHaxeTypesPath,
  getHaxeTypesUri,
  getObfuMap,
  getObfuMapPath,
  getObfuMapUri,
} from "../lib";

describe("getHaxeTypesPath", function () {
  it("returns a string", function () {
    const actual: string = getHaxeTypesPath();
    chai.assert.isString(actual);
  });
});

describe("getHaxeTypesUri", function () {
  it("returns a file URL", function () {
    const actual: url.URL = getHaxeTypesUri();
    chai.assert.instanceOf(actual, url.URL);
    chai.assert.strictEqual(actual.protocol, "file:");
  });
});

describe("getObfuMapPath", function () {
  it("returns a string", function () {
    const actual: string = getObfuMapPath();
    chai.assert.isString(actual);
  });
});

describe("getObfuMapUri", function () {
  it("returns a file URL", function () {
    const actual: url.URL = getObfuMapUri();
    chai.assert.instanceOf(actual, url.URL);
    chai.assert.strictEqual(actual.protocol, "file:");
  });
});

describe("getObfuMap", function () {
  it("returns a non-empty map", async function () {
    const actual: Map<string, string> = await getObfuMap();
    chai.assert.instanceOf(actual, Map);
    chai.assert.isTrue(actual.size > 0, "expected map to be non-empty");
  });
});

describe("haxe", async function () {
  it("compiles types correctly", function () {
    const cmd: string[] = [
      "-main", "TestCompile",
      "-cp", getHaxeTypesPath(),
      "-cp", __dirname,
      "-cp", sysPath.join(__dirname, "..", "..", "..", "node_modules", "@etwin-haxe", "core", "src"),
      "--macro", "include(\"hf\")",
      "-swf-version", "8",
      "-swf", "dummy", "--no-output",
    ];

    const {status, stderr} = childProcess.spawnSync("haxe", cmd);
    chai.assert.strictEqual(status, 0, `Haxe failed (status: ${status}).\nArgs: ${JSON.stringify(cmd)}\n\n${stderr}`);
  });
});
