import fs from "fs";
import sysPath from "path";
import url from "url";
import meta from "./meta.js";

export function getHaxeTypesPath(): string {
  return sysPath.resolve(meta.dirname);
}

export function getHaxeTypesUri(): url.URL {
  return url.pathToFileURL(getHaxeTypesPath());
}

export function getObfuMapPath(): string {
  return sysPath.resolve(meta.dirname, "hf.map.json");
}

export function getObfuMapUri(): url.URL {
  return url.pathToFileURL(getObfuMapPath());
}

export async function getObfuMap(): Promise<Map<string, string>> {
  const obfuMapPath: string = getObfuMapPath();
  const obfuMapStr: string = await readTextAsync(obfuMapPath);
  const obfuMapRecord: Record<string, string> = JSON.parse(obfuMapStr);
  const obfuMap: Map<string, string> = new Map(Object.entries(obfuMapRecord));
  return obfuMap;
}

async function readTextAsync(filePath: fs.PathLike): Promise<string> {
  return new Promise((resolve, reject): void => {
    fs.readFile(
      filePath,
      {encoding: "UTF-8"},
      (err: NodeJS.ErrnoException | null, data: string): void => {
        if (err !== null) {
          reject(err);
        } else {
          resolve(data);
        }
      },
    );
  });
}
