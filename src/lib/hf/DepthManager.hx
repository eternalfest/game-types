package hf;

import etwin.flash.MovieClip;

typedef Plan = {
  var tbl: Array<MovieClip>;
  var cur: Int;
}

extern class DepthManager {
  public var root_mc: MovieClip;
  public var plans: Array<Plan>;

  public function new(mc: MovieClip): Void;

  public function getMC(): MovieClip;

  public function getPlan(pnb: Int): Plan;

  public function compact(plan: Int): Void;

  public function attach(inst: String, plan: Int): MovieClip;

  public function empty(plan: Int): MovieClip;

  public function reserve(mc: MovieClip, plan: Int): Int;

  public function swap(mc: MovieClip, plan: Int): Void;

  public function under(mc: MovieClip): Void;

  public function over(mc: MovieClip): Void;

  public function ysort(plan: Int): Void;

  public function clear(plan: Int): Void;

  public function destroy(): Void;
}
