package hf;

import etwin.flash.MovieClip;
import hf.GameManager;
import hf.Hash;

extern class GameParameters {
  public var root: MovieClip;
  public var manager: GameManager;
  public var options: Hash;
  public var optionList: Array<String>;
  public var families: Array<String>;
  public var scoreItemFamilies: Array<Int>;
  public var specialItemFamilies: Array<Int>;
  public var generalVolume: Float;
  public var soundVolume: Float;
  public var musicVolume: Float;
  public var fl_detail: Bool;
  public var fl_shaky: Bool;

  public function new(mc: MovieClip, man: GameManager, f: String, opt: String): Void;

  public function getStr(n: String): Dynamic;

  public function getInt(n: String): Int;

  public function getBool(n: String): Bool;

  public function setLowDetails(): Void;

  public function hasFamily(id: Int): Bool;

  public function hasOption(oid: String): Bool;

  public function toString(): String;

  public function hasMusic(): Bool;
}
