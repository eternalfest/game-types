package hf;

import hf.entity.item.SpecialItem;
import hf.mode.GameMode;

extern class StatsManager {
  public var game: GameMode;
  public var stats: Array<Stat>;
  public var extendList: Null<Array<Int>>;

  public function new(g: GameMode): Void;

  public function read(id: Int): Int;

  public function getTotal(id: Int): Int;

  public function write(id: Int, n: Int): Void;

  public function inc(id: Int, n: Int): Void;

  public function reset(): Void;

  public function countExtend(): Int;

  public function spreadExtend(): Void;

  public function attachExtend(): SpecialItem;
}
