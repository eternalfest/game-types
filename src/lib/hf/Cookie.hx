package hf;

import etwin.flash.SharedObject;
import hf.GameManager;

extern class Cookie {
  public static var NAME: String;
  public static var VERSION: Int;

  public var manager: GameManager;
  public var cookie: SharedObject;
  public var data: Dynamic /*TODO*/;

  public function new(m: GameManager): Void;

  public function reset(): Void;

  public function flush(): Void;

  public function checkVersion(): Void;

  public function saveSet(name: String, raw: String): Void;

  public function readSet(name: String): String;
}
