package hf;

extern class Chrono {
  public var frameTimer: Int;
  public var gameTimer: Int;
  public var prevFrameTimer: Null<Int>;
  public var suspendTimer: Null<Int>;
  public var haltedTimer: Int;
  public var fl_stop: Bool;
  public var fl_init: Bool;

  public function new(): Void;

  public function formatTime(t: Int): String;

  public function formatTimeShort(t: Int): String;

  public function get(): Int;

  public function getStr(): String;

  public function getStrShort(): String;

  public function reset(): Void;

  public function start(): Void;

  public function stop(): Void;

  public function timeShift(n: Int): Void;

  public function update(): Void;
}
