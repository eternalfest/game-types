package hf;

import etwin.flash.XMLNode;

extern class Lang {
  public static var strList: Array<String>;
  public static var itemNames: Array<String>;
  public static var familyNames: Array<String>;
  public static var questNames: Array<String>;
  public static var questDesc: Array<String>;
  public static var levelNames: Array<Array<String>>;
  public static var keyNames: Array<String>;
  public static var lang: String;
  public static var fl_debug: Bool;
  public static var doc: Null<XMLNode>;

  public static function init(raw: String): Void;

  public static function _getStringData(parentNode: String, attrName: String): Array<String>;

  public static function _find(doc: XMLNode, name: String): Null<XMLNode>;

  public static function get(id: Int): String;

  public static function getItemName(id: Int): String;

  public static function getFamilyName(id: Int): String;

  public static function getQuestName(id: Int): String;

  public static function getQuestDesc(id: Int): String;

  public static function getLevelName(did: Int, lid: Int): String;

  public static function getSectorName(did: Int, lid: Int): String;

  public static function getKeyName(kid: Int): String;
}
