package hf.levels;

import etwin.flash.MovieClip;
import hf.levels.SetManager;
import hf.levels.View;
import hf.native.BitmapData;

extern class ViewManager extends SetManager {
  public var view: Null<View>;
  public var fake: MovieClip;
  public var depthMan: Null<DepthManager>;
  public var scrollCpt: Float;
  public var scrollDir: Int;
  public var fl_scrolling: Bool;
  public var fl_hscrolling: Bool;
  public var fl_fading: Bool;
  public var fl_hideTiles: Bool;
  public var fl_hideBorders: Bool;
  public var fl_shadow: Bool;
  public var fl_restoring: Bool;
  public var fl_fadeNextTransition: Bool;
  public var darknessFactor: Float;
  public var prevSnap: Null<BitmapData>;

  public function new(m: GameManager, setName: String): Void;

  public function setDepthMan(d: DepthManager): Void;

  public function cloneView(v: View): Void;

  public function createFake(snap: BitmapData): Void;

  public function onTransitionDone(): Void;

  public function onScrollDone(): Void;

  public function onHScrollDone(): Void;

  public function onFadeDone(): Void;

  public function onViewReady(): Void;

  public function createView(id: Int): View;

  public function restoreFrom(snap: BitmapData, lid: Int): Void;

  public function getSnapShot(): BitmapData;

  override public function destroy(): Void;

  override public function onDataReady(): Void;

  override public function onRestoreReady(): Void;

  override public function restore(lid: Int): Void;

  override public function suspend(): Void;

  override public function update(): Void;
}
