package hf.levels;

import etwin.flash.MovieClip;

extern class PortalData {
  public var mc: MovieClip;
  public var cx: Int;
  public var cy: Int;
  public var x: Float;
  public var y: Float;
  public var cpt: Float;

  public function new(mc: MovieClip, cx: Int, cy: Int): Void;
}
