package hf.levels;

extern class Data {
  public var __dollar__map: Array<Array<Int>>;
  public var __dollar__playerX: Int;
  public var __dollar__playerY: Int;
  public var __dollar__skinBg: Int;
  public var __dollar__skinTiles: Int;
  public var __dollar__badList: Array<Dynamic /*TODO*/>;
  public var __dollar__specialSlots: Array<Dynamic /*TODO*/>;
  public var __dollar__scoreSlots: Array<Dynamic /*TODO*/>;
  public var __dollar__script: String;

  public function new(): Void;
}
