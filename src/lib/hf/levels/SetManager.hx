package hf.levels;

import hf.GameManager;
import hf.levels.Data in LevelData;
import hf.levels.TeleporterData;
import hf.mode.GameMode;
import hf.Pos;

extern class SetManager {
  public var manager: GameManager;
  public var fl_read: Array<Bool>;
  public var levels: Array<LevelData>;
  public var raw: Array<String>;
  public var fl_mirror: Bool;
  public var setName: String;
  public var teleporterList: Array<TeleporterData>;
  public var portalList: Array<PortalData>;
  public var csum: Int;
  public var _previous: Null<LevelData>;
  public var _previousId: Null<Int>;
  public var current: Null<LevelData>;
  public var currentId: Null<Int>;

  public function new(m: GameManager, s: String): Void;

  public function destroy(): Void;

  public function overwrite(sdata: String): Void;

  public function rollback(): Void;

  public function rollback_xml(): Void;

  public function showField(td: TeleporterData): Void;

  public function hideField(td: TeleporterData): Void;

  public function suspend(): Void;

  public function restore(lid: Int): Void;

  public function setCurrent(id: Int): Void;

  public function isDataReady(): Bool;

  public function checkDataReady(): Void;

  public function goto(id: Int): Void;

  public function next(): Void;

  public function delete(id: Int): Void;

  public function insert(id: Int, data: LevelData): Void;

  public function push(data: LevelData): Void;

  public function flip(l: LevelData): LevelData;

  public function flipPortals(): Void;

  public function isEmptyLevel(id: Int, g: GameMode): Bool;

  public function getCase(pt: Pos<Int>): Int;

  public function forceCase(cx: Int, cy: Int, t: Int): Void;

  public function inBound(cx: Int, cy: Int): Bool;

  public function shapeInBound(e: Entity): Bool;

  public function getGround(cx: Int, cy: Int): Pos<Int>;

  public function onReadComplete(): Void;

  public function onDataReady(): Void;

  public function onEndOfSet(): Void;

  public function onRestoreReady(): Void;

  public function unserialize(id: Int): LevelData;

  public function serialize(id: Int): String;

  public function serializeExternal(l: LevelData): String;

  public function convertWalls(l: LevelData): Void;

  public function exportCookie(): Void;

  public function importCookie(): Void;

  public function trace(id: Int): Void;

  public function update(): Void;
}
