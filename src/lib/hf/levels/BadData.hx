package hf.levels;

extern class BadData {
  public var __dollar__id: Int;
  public var __dollar__x: Float;
  public var __dollar__y: Float;

  public function new(x: Float, y: Float, id: Int): Void;
}
