package hf.levels;

import etwin.flash.MovieClip;
import hf.DepthManager;

extern class ViewLight {
  public var world: Dynamic /*TODO*/;
  public var depthMan: DepthManager;
  public var levelId: Int;
  public var data: Dynamic /*TODO*/;
  public var scalef: Float;
  public var size: Float;
  public var mc: Null<MovieClip>;

  public static var BASE_SIZE: Float;

  public function new(w: Dynamic /*TODO*/, dm: DepthManager, lid: Int): Void;

  public function scale(f: Float): Void;

  public function attach(vx: Float, vy: Float): Void;

  public function strike(): Void;

  public function destroy(): Void;
}
