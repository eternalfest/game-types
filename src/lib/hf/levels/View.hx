package hf.levels;

import etwin.flash.filters.BitmapFilter;
import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.DepthManager;
import hf.levels.Data in LevelData;
import hf.levels.GameMechanics;
import hf.native.BitmapData;

extern class View {
  /**
   * Reference to the game world.
   *
   * The engine really types it as `hf.levels.SetManager` but uses it in fact as
   * `hf.levels.GameMechanics`.
   */
  public var world: GameMechanics;
  public var depthMan: DepthManager;
  public var viewX: Null<Float>;
  public var viewY: Null<Float>;
  public var xOffset: Float;
  public var fl_attach: Bool;
  public var fl_shadow: Bool;
  public var fl_hideTiles: Bool;
  public var fl_hideBorders: Bool;
  public var tileList: Array<MovieClip>;
  public var gridList: Array<MovieClip>;
  public var _fieldMap: Array<Array<Bool>>;
  public var mcList: Array<MovieClip>;
  public var _sprite_top: MovieClip;
  public var _sprite_back: MovieClip;
  public var _tiles: Null<MovieClip>;
  public var _top: Null<DynamicMovieClip /* TODO */>;
  public var _back: Null<DynamicMovieClip /* TODO */>;
  public var _field: Null<MovieClip>;
  public var _leftBorder: Null<MovieClip>;
  public var _rightBorder: Null<MovieClip>;
  public var _sprite_top_dm: DepthManager;
  public var _sprite_back_dm: DepthManager;
  public var _back_dm: DepthManager;
  public var _top_dm: DepthManager;
  public var _field_dm: DepthManager;
  public var _bg: Null<MovieClip>;
  public var _specialBg: Null<DynamicMovieClip /* TODO */>;
  public var fl_cache: Bool;
  public var fl_fast: Bool;
  public var levelId: Null<Int>;
  public var data: Null<LevelData>;
  public var viewCache: Null<BitmapData>;
  public var tileCache: Null<BitmapData>;
  public var topCache: Null<BitmapData>;
  public var snapShot: Null<BitmapData>;

  public function new(world: GameMechanics, dm: DepthManager): Void;

  public function display(id: Int): Void;

  public function displayCurrent(): Void;

  public function displayExternal(d: LevelData): Void;

  public function scale(ratio: Float): Void;

  public function removeShadows(): Void;

  public function isWall(cx: Int, cy: Int): Bool;

  public static function getTileSkinId(id: Int): Int;

  public static function getColumnSkinId(id: Int): Int;

  public static function buildSkinId(tile: Int, column: Int): Int;

  public function attachTile(sx: Int, sy: Int, wid: Int, skin: Int): Void;

  public function attachColumn(sx: Int, sy: Int, wid: Int, skin: Int): Void;

  public function attachField(sx: Int, sy: Int): Void;

  public function attachBg(): Void;

  public function attachSpecialBg(id: Int, subId: Null<Int>): MovieClip;

  public function detachSpecialBg(): Void;

  public function attach(): Void;

  public function attachBadSpots(): Void;

  public function attachGrid(flag: Int, over: Bool): Void;

  public function detachGrid(): Void;

  public function attachSprite(link: String, x: Float, y: Float, fl_back: Bool): MovieClip;

  public function detach(): Void;

  public function detachLevel(): Void;

  public function detachSprites(): Void;

  public function moveTo(x: Float, y: Float): Void;

  public function setFilter(f: BitmapFilter): Void;

  public function moveToPreviousPos(): Void;

  public function getSnapShot(x: Float, y: Float): BitmapData;

  public function updateSnapShot(): Void;

  public function destroy(): Void;
}
