package hf.levels;

extern class PortalLink {
  public var from_did: Null<Int>;
  public var from_lid: Null<Int>;
  public var from_pid: Null<Int>;
  public var to_did: Null<Int>;
  public var to_lid: Null<Int>;
  public var to_pid: Null<Int>;

  public function new(): Void;

  public function cleanUp(): Void;

  public function trace(): Void;
}
