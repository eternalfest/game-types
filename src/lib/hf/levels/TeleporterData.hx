package hf.levels;

import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;

extern class TeleporterData {
  public var cx: Float;
  public var cy: Float;
  public var dir: Int;
  public var length: Float;
  public var fl_on: Bool;
  public var centerX: Float;
  public var centerY: Float;
  public var startX: Float;
  public var startY: Float;
  public var endX: Float;
  public var endY: Float;
  public var mc: Null<DynamicMovieClip /* TODO */>;
  public var podA: Null<MovieClip>;
  public var podB: Null<MovieClip>;

  public function new(x: Float, y: Float, len: Float, dir: Int): Void;
}
