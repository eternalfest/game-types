package hf.levels;

import etwin.flash.XML;
import etwin.flash.XMLNode;
import hf.levels.Data in LevelData;
import hf.mode.GameMode;
import hf.Case;

typedef ScriptExp = {
  var x: Float;
  var y: Float;
  var r: Float;
}

typedef ScriptMc = {
  var sid: Int;
  var mc: etwin.flash.DynamicMovieClip /* TODO */;
}

extern class ScriptEngine {
  public var game: GameMode;
  public var data: LevelData;
  public var baseScript: String;
  public var extraScript: String;

  /**
   * Compiled script.
   *
   * This field is not set initially. It is set by the `compile` method.
   * The following property is verified: `(script != null) == fl_compile`.
   *
   * A Flash `XML` object.
   */
  public var script: Null<XML>;
  public var cycle: Float;
  public var bads: Int;
  public var fl_compile: Bool;
  public var fl_birth: Bool;
  public var fl_death: Bool;
  public var fl_onAttach: Bool;
  public var fl_safe: Bool;
  public var fl_elevatorOpen: Bool;
  public var fl_firstTorch: Bool;
  /**
   * Flag indicating that the view should be redrawn after the script execution.
   */
  public var fl_redraw: Null<Bool>;
  public var bossDoorTimer: Float;
  public var recentExp: Array<ScriptExp>;
  public var entries: Array<Case>;
  public var mcList: Array<ScriptMc>;
  public var history: Array<String>;

  public function new(g: GameMode, d: LevelData): Void;

  public function destroy(): Void;

  public function traceHistory(str: String): Void;

  public function onPlayerBirth(): Void;

  public function onPlayerDeath(): Void;

  public function onExplode(x: Float, y: Float, radius: Float): Void;

  public function onEnterCase(cx: Int, cy: Int): Void;

  public function onLevelAttach(): Void;

  public function safeMode(): Void;

  public function normalMode(): Void;

  public function isVerbose(t: String): Bool;

  public function getInt(node: XMLNode, name: String): Null<Int>;

  public function getFloat(node: XMLNode, name: String): Null<Float>;

  public function getString(node: XMLNode, name: String): Null<String>;

  public function addScript(str: String): Void;

  public function addNode(name: String, att: String, inner: String): Void;

  public function addShortNode(name: String, att: String): Void;

  public function executeEvent(event: XMLNode): Void;

  public function isTrigger(n: String): Bool;

  public function checkTrigger(trigger: XMLNode): Bool;

  public function executeTrigger(trigger: XMLNode): Void;

  public function insertBads(): Int;

  public function insertItem(event: String, id: Int, subId: Int, x: Float, y: Float, t: Float, repeat: Int, fl_inf: Bool, fl_clearAtEnd: Bool): Void;

  public function insertSpecialItem(id: Int, sid: Int, x: Float, y: Float, t: Float, repeat: Int, fl_inf: Bool, fl_clearAtEnd: Bool): Void;

  public function insertScoreItem(id: Int, sid: Int, x: Float, y: Float, t: Float, repeat: Int, fl_inf: Bool, fl_clearAtEnd: Bool): Void;

  public function insertExtend(): Void;

  public function insertPortal(cx: Float, cy: Float, pid: Int): Void;

  public function runScript(): Void;

  public function compile(): Void;

  public function clearScript(): Void;

  public function clearEndTriggers(): Void;

  public function reset(): Void;

  public function killById(id: Int): Void;

  public function playById(id: Int): Void;

  public function codeTrigger(id: Int): Void;

  public function update(): Void;

  public static var T_TIMER: String;
  public static var T_POS: String;
  public static var T_ATTACH: String;
  public static var T_DO: String;
  public static var T_END: String;
  public static var T_BIRTH: String;
  public static var T_DEATH: String;
  public static var T_EXPLODE: String;
  public static var T_ENTER: String;
  public static var T_NIGHTMARE: String;
  public static var T_MIRROR: String;
  public static var T_MULTI: String;
  public static var T_NINJA: String;
  public static var E_SCORE: String;
  public static var E_SPECIAL: String;
  public static var E_EXTEND: String;
  public static var E_BAD: String;
  public static var E_KILL: String;
  public static var E_TUTORIAL: String;
  public static var E_MESSAGE: String;
  public static var E_KILLMSG: String;
  public static var E_POINTER: String;
  public static var E_KILLPTR: String;
  public static var E_MC: String;
  public static var E_PLAYMC: String;
  public static var E_MUSIC: String;
  public static var E_ADDTILE: String;
  public static var E_REMTILE: String;
  public static var E_ITEMLINE: String;
  public static var E_GOTO: String;
  public static var E_HIDE: String;
  public static var E_HIDEBORDERS: String;
  public static var E_CODETRIGGER: String;
  public static var E_PORTAL: String;
  public static var E_SETVAR: String;
  public static var E_OPENPORTAL: String;
  public static var E_DARKNESS: String;
  public static var E_FAKELID: String;
  public static var VERBOSE_TRIGGERS: Array<String>;
}
