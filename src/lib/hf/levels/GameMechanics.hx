package hf.levels;

import hf.Entity;
import hf.entity.Physics;
import hf.GameManager;
import hf.levels.Data in LevelData;
import hf.levels.ScriptEngine;
import hf.levels.TeleporterData;
import hf.levels.ViewManager;
import hf.mode.GameMode;
import hf.Case;
import hf.Pos;

typedef NextTeleporter = {
  var fl_rand: Bool;
  var td: TeleporterData;
}

extern class GameMechanics extends ViewManager {
  public var fl_parsing: Bool;
  public var fl_lock: Bool;
  public var fl_visited: Array<Bool>;
  public var fl_mainWorld: Bool;
  /**
   * triggers[x][y]
   */
  public var triggers: Array<Array<Array<Entity>>>;
  public var scriptEngine: Null<ScriptEngine>;
  public var game: Null<GameMode>;
  public var flcurrentIA: Null<Bool>;
  public var _iteration: Case;
  public var flagMap: Array<Array<Int>>;
  public var fallMap: Array<Array<Int>>;

  public function new(m: GameManager, s: String): Void;

  public function setGame(g: GameMode): Void;

  public function lock(): Void;

  public function unlock(): Void;

  public function setVisited(): Void;

  public function isVisited(): Bool;

  public function resetIA(): Void;

  public function checkFlag(pt: Pos<Int>, flag: Int): Bool;

  public function forceFlag(pt: Pos<Int>, flag: Int, value: Bool): Void;

  public function parseCurrentIA(it: Case): Void;

  public function _checkSecureFall(cx: Int, cy: Int): Int;

  public function getWallHeight(cx: Int, cy: Int, max: Int): Null<Int>;

  public function getStepHeight(cx: Int, cy: Int, max: Int): Null<Int>;

  public function onParseIAComplete(): Void;

  public function parseWalls(l: LevelData): Void;

  public function getTeleporter(e: Physics, cx: Int, cy: Int): Null<TeleporterData>;

  public function getNextTeleporter(start: TeleporterData): NextTeleporter;

  override public function createView(id: Int): View;

  override public function destroy(): Void;

  override public function forceCase(cx: Int, cy: Int, t: Int): Void;

  override public function goto(id: Int): Void;

  override public function isDataReady(): Bool;

  override public function onDataReady(): Void;

  override public function onEndOfSet(): Void;

  override public function onFadeDone(): Void;

  override public function onHScrollDone(): Void;

  override public function onReadComplete(): Void;

  override public function onRestoreReady(): Void;

  override public function onViewReady(): Void;

  override public function restore(lid: Int): Void;

  override public function suspend(): Void;

  override public function unserialize(id: Int): LevelData;

  override public function update(): Void;
}
