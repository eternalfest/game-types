package hf;

extern class Codec {
  public var key: Dynamic /*TODO*/;
  public var ikey: Int;
  public var pkey: Null<Int>;
  public var crc: Null<Int>;
  public var str: Null<String>;

  public static var BASE64: String;
  public static var IDCHARS: String;

  public function new(key: Dynamic /*TODO*/): Void;

  public function makeCrcString(): String;

  public function encode(o: Dynamic): String;

  public function decode(s: String): Dynamic;

  public function writeStr(s: String): Void;

  public function writeChar(c: String): Void;

  public function readChar(): String;

  public function readStr(): String;

  public function encodeArray(o: Array<Dynamic>): Void;

  public function encodeObject(o: Dynamic): Void;

  public function encodeAny(o: Dynamic): Void;

  public function decodeArray(): Array<Dynamic>;

  public function decodeObject(): Dynamic;

  public function decodeAny(): Dynamic;
}
