package hf.interf;

import etwin.flash.MovieClip;
import etwin.flash.TextField;

/**
 * Class representing the `hammer_interf_item_name` sprite.
 */
@:interface
extern class ItemName extends MovieClip {
  public var field: TextField;
}
