package hf.flash;

import etwin.flash.geom.Matrix;
import etwin.flash.MovieClip;

extern class Init {
  public static var __m: Matrix;
  public static var __tx: String;
  public static var __ty: String;

  public function new(): Void;

  public function drawMC(mc: MovieClip, x: Float, y: Float): Void;

  public static function init(): Void;
}
