package hf;

import etwin.flash.XMLSocket;

extern class FileServer {
  public var connected: Bool;
  public var commands: Array<String>;
  public var s: XMLSocket;
  public var onChange: Null<Void -> Void>;
  public var onLoad: Null<Null<String> -> Void>;

  public static var HOST: String;
  public static var PATH: String;
  public static var PORT: Int;

  public function new(key: Dynamic /*TODO*/): Void;

  public static function initPath(): String;

  public function connect(): Void;

  public function close(): Void;

  public function sendCommand(x: String): Void;

  public function onConnectionResult(b: Bool): Void;

  public function onReceivedData(d: Dynamic /*TODO*/): Void;

  public function load(file: String): Void;

  public function save(file: String, data: String): Void;
}
