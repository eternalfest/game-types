package hf;

import hf.GameManager;
import hf.Hash;
import hf.levels.PortalLink;
import hf.entity.Animator.Anim;

typedef ItemInfo = {
  var id: Int;
  var r: Int;
  var br: Int;
  var v: Int;
  var name: String;
}

typedef LevelRef = {
  var did: Int;
  var lid: Int;
}

typedef LevelTag = {
  var name: String;
  var did: Int;
  var lid: Int;
}

extern class Data {
  /**
   * Current game manager.
   */
  public static var manager: Null<GameManager>;
  // TODO: ArrayMap<Array<ItemInfo>>
  public static var SPECIAL_ITEM_FAMILIES: Null<Array<Array<ItemInfo>>>;
  // TODO: ArrayMap<Array<ItemInfo>>
  public static var SCORE_ITEM_FAMILIES: Null<Array<Array<ItemInfo>>>;
  public static var FAMILY_CACHE: Null<Array<Int>>;
  public static var ITEM_VALUES: Null<Dynamic>;
  public static var LINKS: Null<Array<PortalLink>>;
  public static var LEVEL_TAG_LIST: Null<Array<LevelTag>>;

  public static function initLinkages(): Array<String>;

  public static function init(m: GameManager): Void;

  public static function initItemsRaw(): Array<Int>;

  // TODO: ArrayMap<Array<ItemInfo>>
  public static function xml_readFamily(xmlName: String): Array<Array<ItemInfo>>;

  // TODO: ArrayMap<Array<ItemInfo>>
  public static function xml_readSpecialItems(): Array<Array<ItemInfo>>;

  // TODO: ArrayMap<Array<ItemInfo>>
  public static function xml_readScoreItems(): Array<Array<ItemInfo>>;

  public static function getRandFromFamilies(familySet: Array<Array<ItemInfo>>, familiesId: Array<Int>): Array<Int>;

  public static function getScoreItemValues(): Array<Int>;

  public static function cacheFamilies(): Array<Int>;

  public static function getTagFromLevel(did: Int, lid: Int): Null<String>;

  public static function getLevelFromTag(code: String): LevelRef;

  public static function xml_readPortalLinks(): Array<PortalLink>;

  public static function computeMd5(fam: Array<Dynamic /*TODO*/>): String;

  public static function serializeHash(h: Hash): String;

  public static function unserializeHash(str: String): Hash;

  // TODO: Generic
  public static function duplicate(o: Dynamic): Dynamic;

  public static function getCrystalValue(id: Int): Int;

  public static function getCrystalTime(id: Int): Int;

  public static function cleanLeading(s: String): String;

  public static function cleanString(s: String): String;

  public static function leadingZeros(n: Int, zeros: Int): String;

  public static function replaceTag(str: String, char: String, start: String, end: String): String;

  public static function stime(): Void;

  public static function time(n: Int): Void;

  public static function formatNumber(n: Int): String;

  public static function formatNumberStr(txt: String): String;

  public static function getLink(did: Int, lid: Int, pid: Int): PortalLink;

  /**
   * Width of the Flash document, in pixels.
   */
  public static var DOC_WIDTH: Int;

  /**
   * Height of the Flash document, in pixels.
   */
  public static var DOC_HEIGHT: Int;

  /**
   * Width of the game area, in pixels.
   */
  public static var GAME_WIDTH: Int;

  /**
   * Height of the game area, in pixels.
   */
  public static var GAME_HEIGHT: Int;

  /**
   * Level width, in cases.
   */
  public static var LEVEL_WIDTH: Int;

  /**
   * Level height, in cases.
   */
  public static var LEVEL_HEIGHT: Int;

  /**
   * Case width, in pixels.
   */
  public static var CASE_WIDTH: Int;

  /**
   * Case height. in pixels.
   */
  public static var CASE_HEIGHT: Int;

  /**
   * Duration of a second, in frames.
   */
  public static var SECOND: Int;

  public static var auto_inc: Int;
  public static var DP_SPECIAL_BG: Int;
  public static var DP_BACK_LAYER: Int;
  public static var DP_SPRITE_BACK_LAYER: Int;
  public static var DP_FIELD_LAYER: Int;
  public static var DP_SPEAR: Int;
  public static var DP_PLAYER: Int;
  public static var DP_ITEMS: Int;
  public static var DP_SHOTS: Int;
  public static var DP_BADS: Int;
  public static var DP_BOMBS: Int;
  public static var DP_FX: Int;
  public static var DP_SUPA: Int;
  public static var DP_TOP_LAYER: Int;
  public static var DP_SPRITE_TOP_LAYER: Int;
  public static var DP_BORDERS: Int;
  public static var DP_SCROLLER: Int;
  public static var DP_INTERF: Int;
  public static var DP_TOP: Int;
  public static var DP_SOUNDS: Int;
  public static var CHAN_MUSIC: Int;
  public static var CHAN_BOMB: Int;
  public static var CHAN_PLAYER: Int;
  public static var CHAN_BAD: Int;
  public static var CHAN_ITEM: Int;
  public static var CHAN_FIELD: Int;
  public static var CHAN_INTERF: Int;
  public static var TRACKS: Array<String>;
  public static var type_bit: Int;
  public static var ENTITY: Int;
  public static var PHYSICS: Int;
  public static var ITEM: Int;
  public static var SPECIAL_ITEM: Int;
  public static var PLAYER: Int;
  public static var PLAYER_BOMB: Int;
  public static var BAD: Int;
  public static var SHOOT: Int;
  public static var BOMB: Int;
  public static var FX: Int;
  public static var SUPA: Int;
  public static var CATCHER: Int;
  public static var BALL: Int;
  public static var HU_BAD: Int;
  public static var BOSS: Int;
  public static var BAD_BOMB: Int;
  public static var BAD_CLEAR: Int;
  public static var SOCCERBALL: Int;
  public static var PLAYER_SHOOT: Int;
  public static var PERFECT_ITEM: Int;
  public static var SPEAR: Int;
  public static var GROUND: Int;
  public static var WALL: Int;
  public static var OUT_WALL: Int;
  public static var HORIZONTAL: Int;
  public static var VERTICAL: Int;
  public static var SCROLL_SPEED: Float;
  public static var FADE_SPEED: Float;
  public static var FIELD_TELEPORT: Int;
  public static var FIELD_PORTAL: Int;
  public static var FIELD_GOAL_1: Int;
  public static var FIELD_GOAL_2: Int;
  public static var FIELD_BUMPER: Int;
  public static var FIELD_PEACE: Int;
  public static var HU_STEPS: Array<Float>;
  public static var LEVEL_READ_LENGTH: Int;
  public static var MIN_DARKNESS_LEVEL: Int;
  public static var stat_inc: Int;
  public static var STAT_MAX_COMBO: Int;
  public static var STAT_SUPAITEM: Int;
  public static var STAT_KICK: Int;
  public static var STAT_BOMB: Int;
  public static var STAT_SHOT: Int;
  public static var STAT_JUMP: Int;
  public static var STAT_ICEHIT: Int;
  public static var STAT_KNOCK: Int;
  public static var STAT_DEATH: Int;
  public static var EXTEND_TIMER: Float;
  public static var EXT_MIN_COMBO: Int;
  public static var EXT_MAX_BOMBS: Int;
  public static var EXT_MAX_KICKS: Int;
  public static var EXT_MAX_JUMPS: Int;
  public static var BLINK_DURATION: Float;
  public static var BLINK_DURATION_FAST: Float;
  public static var ANIM_PLAYER_STOP: Anim;
  public static var ANIM_PLAYER_WALK: Anim;
  public static var ANIM_PLAYER_JUMP_UP: Anim;
  public static var ANIM_PLAYER_JUMP_DOWN: Anim;
  public static var ANIM_PLAYER_JUMP_LAND: Anim;
  public static var ANIM_PLAYER_DIE: Anim;
  public static var ANIM_PLAYER_KICK: Anim;
  public static var ANIM_PLAYER_ATTACK: Anim;
  public static var ANIM_PLAYER_EDGE: Anim;
  public static var ANIM_PLAYER_WAIT1: Anim;
  public static var ANIM_PLAYER_WAIT2: Anim;
  public static var ANIM_PLAYER_KNOCK_IN: Anim;
  public static var ANIM_PLAYER_KNOCK_OUT: Anim;
  public static var ANIM_PLAYER_RESURRECT: Anim;
  public static var ANIM_PLAYER_CARROT: Anim;
  public static var ANIM_PLAYER_RUN: Anim;
  public static var ANIM_PLAYER_SOCCER: Anim;
  public static var ANIM_PLAYER_AIRKICK: Anim;
  public static var ANIM_PLAYER_STOP_V: Anim;
  public static var ANIM_PLAYER_WALK_V: Anim;
  public static var ANIM_PLAYER_STOP_L: Anim;
  public static var ANIM_PLAYER_WALK_L: Anim;
  public static var ANIM_BAD_WALK: Anim;
  public static var ANIM_BAD_ANGER: Anim;
  public static var ANIM_BAD_FREEZE: Anim;
  public static var ANIM_BAD_KNOCK: Anim;
  public static var ANIM_BAD_DIE: Anim;
  public static var ANIM_BAD_SHOOT_START: Anim;
  public static var ANIM_BAD_SHOOT_END: Anim;
  public static var ANIM_BAD_THINK: Anim;
  public static var ANIM_BAD_JUMP: Anim;
  public static var ANIM_BAD_SHOOT_LOOP: Anim;
  public static var ANIM_BAT_WAIT: Anim;
  public static var ANIM_BAT_MOVE: Anim;
  public static var ANIM_BAT_SWITCH: Anim;
  public static var ANIM_BAT_DIVE: Anim;
  public static var ANIM_BAT_INTRO: Anim;
  public static var ANIM_BAT_KNOCK: Anim;
  public static var ANIM_BAT_FINAL_DIVE: Anim;
  public static var ANIM_BAT_ANGER: Anim;
  public static var ANIM_BOSS_WAIT: Anim;
  public static var ANIM_BOSS_SWITCH: Anim;
  public static var ANIM_BOSS_JUMP_UP: Anim;
  public static var ANIM_BOSS_JUMP_DOWN: Anim;
  public static var ANIM_BOSS_JUMP_LAND: Anim;
  public static var ANIM_BOSS_TORNADO_START: Anim;
  public static var ANIM_BOSS_TORNADO_END: Anim;
  public static var ANIM_BOSS_BAT_FORM: Anim;
  public static var ANIM_BOSS_BURN_START: Anim;
  public static var ANIM_BOSS_DEATH: Anim;
  public static var ANIM_BOSS_DASH_START: Anim;
  public static var ANIM_BOSS_DASH: Anim;
  public static var ANIM_BOSS_BOMB: Anim;
  public static var ANIM_BOSS_HIT: Anim;
  public static var ANIM_BOSS_DASH_BUILD: Anim;
  public static var ANIM_BOSS_BURN_LOOP: Anim;
  public static var ANIM_BOSS_TORNADO_LOOP: Anim;
  public static var ANIM_BOSS_DASH_LOOP: Anim;
  public static var ANIM_BOMB_DROP: Anim;
  public static var ANIM_BOMB_LOOP: Anim;
  public static var ANIM_BOMB_EXPLODE: Anim;
  public static var ANIM_WBOMB_STOP: Anim;
  public static var ANIM_WBOMB_WALK: Anim;
  public static var ANIM_SHOOT: Anim;
  public static var ANIM_SHOOT_LOOP: Anim;
  public static var MAX_ITERATION: Int;
  public static var flag_bit: Int;
  public static var GRID_NAMES: Array<String>;
  public static var IA_TILE_TOP: Int;
  public static var IA_ALLOW_FALL: Int;
  public static var IA_BORDER: Int;
  public static var IA_SMALL_SPOT: Int;
  public static var IA_FALL_SPOT: Int;
  public static var IA_JUMP_UP: Int;
  public static var IA_JUMP_DOWN: Int;
  public static var IA_JUMP_LEFT: Int;
  public static var IA_JUMP_RIGHT: Int;
  public static var IA_TILE: Int;
  public static var IA_CLIMB_LEFT: Int;
  public static var IA_CLIMB_RIGHT: Int;
  public static var FL_TELEPORTER: Int;
  public static var IA_HJUMP: Int;
  public static var IA_VJUMP: Int;
  public static var IA_CLIMB: Int;
  public static var IA_CLOSE_DISTANCE: Float;
  public static var ACTION_MOVE: Int;
  public static var ACTION_WALK: Int;
  public static var ACTION_SHOOT: Int;
  public static var ACTION_FALLBACK: Int;
  public static var EVENT_EXIT_RIGHT: Int;
  public static var EVENT_BACK_RIGHT: Int;
  public static var EVENT_EXIT_LEFT: Int;
  public static var EVENT_BACK_LEFT: Int;
  public static var EVENT_DEATH: Int;
  public static var EVENT_EXTEND: Int;
  public static var EVENT_TIME: Int;
  public static var BORDER_MARGIN: Float;
  public static var FRICTION_X: Float;
  public static var FRICTION_Y: Float;
  public static var FRICTION_GROUND: Float;
  public static var FRICTION_SLIDE: Float;
  public static var GRAVITY: Float;
  public static var FALL_FACTOR_FROZEN: Float;
  public static var FALL_FACTOR_KNOCK: Float;
  public static var FALL_FACTOR_DEAD: Float;
  public static var FALL_SPEED: Float;
  public static var STEP_MAX: Float;
  public static var DEATH_LINE: Float;
  public static var MAX_FX: Int;
  public static var DUST_FALL_HEIGHT: Float;
  public static var PARTICLE_ICE: Int;
  public static var PARTICLE_CLASSIC_BOMB: Int;
  public static var PARTICLE_STONE: Int;
  public static var PARTICLE_SPARK: Int;
  public static var PARTICLE_DUST: Int;
  public static var PARTICLE_ORANGE: Int;
  public static var PARTICLE_METAL: Int;
  public static var PARTICLE_TUBERCULOZ: Int;
  public static var PARTICLE_RAIN: Int;
  public static var PARTICLE_LITCHI: Int;
  public static var PARTICLE_PORTAL: Int;
  public static var PARTICLE_BUBBLE: Int;
  public static var PARTICLE_ICE_BAD: Int;
  public static var PARTICLE_BLOB: Int;
  public static var PARTICLE_FRAMB: Int;
  public static var PARTICLE_FRAMB_SMALL: Int;
  public static var BG_STAR: Int;
  public static var BG_FLASH: Int;
  public static var BG_ORANGE: Int;
  public static var BG_FIREBALL: Int;
  public static var BG_HYPNO: Int;
  public static var BG_CONSTEL: Int;
  public static var BG_JAP: Int;
  public static var BG_GHOSTS: Int;
  public static var BG_FIRE: Int;
  public static var BG_PYRAMID: Int;
  public static var BG_SINGER: Int;
  public static var BG_STORM: Int;
  public static var BG_GUU: Int;
  public static var BG_SOCCER: Int;
  public static var PLAYER_SPEED: Float;
  public static var PLAYER_JUMP: Float;
  public static var PLAYER_HKICK_X: Float;
  public static var PLAYER_HKICK_Y: Float;
  public static var PLAYER_VKICK: Float;
  public static var PLAYER_AIR_JUMP: Float;
  public static var WBOMB_SPEED: Float;
  public static var KICK_DISTANCE: Float;
  public static var AIR_KICK_DISTANCE: Float;
  public static var SHIELD_DURATION: Float;
  public static var WEAPON_DURATION: Float;
  public static var SUPA_DURATION: Float;
  public static var EXTRA_LIFE_STEPS: Array<Int>;
  public static var TELEPORTER_DISTANCE: Float;
  public static var BASE_COLORS: Array<Int>;
  public static var DARK_COLORS: Array<Int>;
  public static var CURSE_PEACE: Int;
  public static var CURSE_SHRINK: Int;
  public static var CURSE_SLOW: Int;
  public static var CURSE_TAUNT: Int;
  public static var CURSE_MULTIPLY: Int;
  public static var CURSE_FALL: Int;
  public static var CURSE_MARIO: Int;
  public static var CURSE_TRAITOR: Int;
  public static var CURSE_GOAL: Int;
  public static var EDGE_TIMER: Float;
  public static var WAIT_TIMER: Float;
  public static var WEAPON_B_CLASSIC: Int;
  public static var WEAPON_B_BLACK: Int;
  public static var WEAPON_B_BLUE: Int;
  public static var WEAPON_B_GREEN: Int;
  public static var WEAPON_B_RED: Int;
  public static var WEAPON_B_REPEL: Int;
  public static var WEAPON_NONE: Int;
  public static var WEAPON_S_ARROW: Int;
  public static var WEAPON_S_FIRE: Int;
  public static var WEAPON_S_ICE: Int;
  public static var HEAD_NORMAL: Int;
  public static var HEAD_AFRO: Int;
  public static var HEAD_CERBERE: Int;
  public static var HEAD_PIOU: Int;
  public static var HEAD_MARIO: Int;
  public static var HEAD_TUB: Int;
  public static var HEAD_IGORETTE: Int;
  public static var HEAD_LOSE: Int;
  public static var HEAD_CROWN: Int;
  public static var HEAD_SANDY: Int;
  public static var HEAD_SANDY_LOSE: Int;
  public static var HEAD_SANDY_CROWN: Int;
  public static var PEACE_COOLDOWN: Float;
  public static var AUTO_ANGER: Float;
  public static var MAX_ANGER: Int;
  public static var BALL_TIMEOUT: Float;
  public static var BAD_HJUMP_X: Float;
  public static var BAD_HJUMP_Y: Float;
  public static var BAD_VJUMP_X_CLIFF: Float;
  public static var BAD_VJUMP_X: Float;
  public static var BAD_VJUMP_Y: Float;
  public static var BAD_VJUMP_Y_LIST: Array<Float>;
  public static var BAD_VDJUMP_Y: Float;
  public static var FREEZE_DURATION: Float;
  public static var KNOCK_DURATION: Float;
  public static var PLAYER_KNOCK_DURATION: Float;
  public static var ICE_HIT_MIN_SPEED: Float;
  public static var ICE_KNOCK_MIN_SPEED: Float;
  public static var BAD_POMME: Int;
  public static var BAD_CERISE: Int;
  public static var BAD_BANANE: Int;
  public static var BAD_FIREBALL: Int;
  public static var BAD_ANANAS: Int;
  public static var BAD_ABRICOT: Int;
  public static var BAD_ABRICOT2: Int;
  public static var BAD_POIRE: Int;
  public static var BAD_BOMBE: Int;
  public static var BAD_ORANGE: Int;
  public static var BAD_FRAISE: Int;
  public static var BAD_CITRON: Int;
  public static var BAD_BALEINE: Int;
  public static var BAD_SPEAR: Int;
  public static var BAD_CRAWLER: Int;
  public static var BAD_TZONGRE: Int;
  public static var BAD_SAW: Int;
  public static var BAD_LITCHI: Int;
  public static var BAD_KIWI: Int;
  public static var BAD_LITCHI_WEAK: Int;
  public static var BAD_FRAMBOISE: Int;
  public static var LINKAGES: Array<String>;
  public static var BAT_LEVEL: Int;
  public static var TUBERCULOZ_LEVEL: Int;
  public static var BOSS_BAT_MIN_DIST: Float;
  public static var BOSS_BAT_MIN_X_DIST: Float;
  public static var MAX_ITEMS: Int;
  public static var ITEM_LIFE_TIME: Float;
  public static var DIAMANT: Int;
  public static var CONVERT_DIAMANT: Int;
  public static var EXTENDS: Array<String>;
  public static var SPECIAL_ITEM_TIMER: Float;
  public static var SCORE_ITEM_TIMER: Float;
  public static var __NA: Int;
  public static var COMM: Int;
  public static var UNCO: Int;
  public static var RARE: Int;
  public static var UNIQ: Int;
  public static var MYTH: Int;
  public static var CANE: Int;
  public static var LEGEND: Int;
  public static var RARITY: Array<Int>;
  public static var RAND_EXTENDS_ID: Int;
  public static var RAND_ITEMS_ID: Int;
  public static var RAND_SCORES_ID: Int;
  public static var RAND_EXTENDS: Array<Int>;
  public static var OPT_MIRROR: String;
  public static var OPT_MIRROR_MULTI: String;
  public static var OPT_NIGHTMARE_MULTI: String;
  public static var OPT_NIGHTMARE: String;
  public static var OPT_LIFE_SHARING: String;
  public static var OPT_SOCCER_BOMBS: String;
  public static var OPT_KICK_CONTROL: String;
  public static var OPT_BOMB_CONTROL: String;
  public static var OPT_NINJA: String;
  public static var OPT_BOMB_EXPERT: String;
  public static var OPT_BOOST: String;
  public static var OPT_SET_TA_0: String;
  public static var OPT_SET_TA_1: String;
  public static var OPT_SET_TA_2: String;
  public static var OPT_SET_MTA_0: String;
  public static var OPT_SET_MTA_1: String;
  public static var OPT_SET_MTA_2: String;
  public static var OPT_SET_SOC_0: String;
  public static var OPT_SET_SOC_1: String;
  public static var OPT_SET_SOC_2: String;
  public static var OPT_SET_SOC_3: String;
  public static var FIELDS: Array<Null<String>>;
  public static var MAX_TILES: Int;
  public static var MAX_BG: Int;
  public static var MAX_BADS: Int;
  public static var MAX_FIELDS: Int;
  public static var TOOL_TILE: Int;
  public static var TOOL_BAD: Int;
  public static var TOOL_FIELD: Int;
  public static var TOOL_START: Int;
  public static var TOOL_SPECIAL: Int;
  public static var TOOL_SCORE: Int;
  public static var WATCH: Int;
}
