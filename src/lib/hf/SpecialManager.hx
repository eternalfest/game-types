package hf;

import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.entity.item.SpecialItem;
import hf.entity.Player;
import hf.mode.GameMode;

typedef RecEffect = {
  var timer: Float;
  var baseTimer: Float;
  var func: Void -> Void;
  var fl_repeat: Bool;
}

typedef TempEffect = {
  var id: Int;
  var end: Float;
}

extern class SpecialManager {
  public var game: GameMode;
  public var player: Player;
  public var permList: Array<Int>;
  public var tempList: Array<TempEffect>;
  public var actives: Array<Bool>;
  public var recurring: Array<RecEffect>;
  public var clouds: Array<MovieClip>;
  public var phoneMC: DynamicMovieClip /* TODO */;

  public function new(g: GameMode, p: Player): Void;

  public function permanent(id: Int): Void;

  public function temporary(id: Int, duration: Null<Float>): Void;

  public function global(id: Int): Void;

  public function clearTemp(): Void;

  public function clearPerm(): Void;

  public function clearRec(): Void;

  public function registerRecurring(func: Void -> Void, t: Float, fl_repeat: Bool): Void;

  public function levelConversion(id: Int, sid: Int): Void;

  public function getZodiac(id: Int): Void;

  public function getZodiacPotion(id: Int): Void;

  public function onPerfect(): Void;

  public function onPickPerfectItem(): Void;

  public function executeExtend(fl_perfect: Bool): Void;

  public function warpZone(w: Int): Void;

  public function execute(item: SpecialItem): Void;

  public function interrupt(id: Int): Void;

  public function onStrike(): Void;

  public function onFireRain(): Void;

  public function onPoT(): Void;

  public function main(): Void;
}
