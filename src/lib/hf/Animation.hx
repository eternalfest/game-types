package hf;

import etwin.flash.DynamicMovieClip;
import hf.mode.GameMode;

extern class Animation {
  public var mc: Null<DynamicMovieClip /* TODO */>;
  public var game: GameMode;
  public var frame: Float;
  public var lifeTimer: Float;
  public var blinkTimer: Float;
  public var fl_kill: Bool;
  public var fl_loop: Bool;
  public var fl_loopDone: Bool;
  public var fl_blink: Bool;

  public function new(g: GameMode): Void;

  public function attach(x: Float, y: Float, link: String, depth: Int): Void;

  public function init(g: GameMode): Void;

  public function destroy(): Void;

  public function blink(): Void;

  public function stopBlink(): Void;

  public function short(): String;

  public function update(): Void;
}
