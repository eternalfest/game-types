package hf;

import etwin.flash.MovieClip;
import hf.DepthManager;
import hf.GameManager;
import hf.Hf;
import hf.SoundManager;

extern class Mode {
  public var _name: String;
  public var manager: GameManager;
  public var root: Hf;
  public var mc: MovieClip;
  public var depthMan: DepthManager;
  public var soundMan: SoundManager;
  public var fl_switch: Bool;
  public var fl_hide: Bool;
  public var fl_lock: Bool;
  public var fl_music: Bool;
  public var fl_mute: Bool;
  public var fl_runAsChild: Bool;
  public var currentTrack: Null<Int>;
  public var xOffset: Float;
  public var yOffset: Float;
  public var uniqId: Int;
  public var cycle: Float;
  public var xFriction: Null<Float>;
  public var yFriction: Null<Float>;

  public function new(m: GameManager): Void;

  public function show(): Void;

  public function hide(): Void;

  public function init(): Void;

  public function getUniqId(): Int;

  public function lock(): Void;

  public function unlock(): Void;

  public function short(): String;

  public function destroy(): Void;

  public function updateConstants(): Void;

  public function getDebugControls(): Void;

  public function getControls(): Void;

  public function onSleep(): Void;

  public function onWakeUp(modeName: String, data: Dynamic /*TODO*/): Void;

  public function playMusic(id: Int): Void;

  public function playMusicAt(id: Int, pos: Float): Void;

  public function stopMusic(): Void;

  public function setMusicVolume(n: Float): Void;

  public function endMode(): Void;

  public function main(): Void;
}
