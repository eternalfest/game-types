package hf;

import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.Animation;
import hf.mode.GameMode;

typedef DelayedFx = {
  var t: Float;
  var x: Float;
  var y: Float;
  var link: String;
}

typedef SpecialBg = {
  var id: Int;
  var subId: Int;
  var timer: Float;
}

extern class FxManager {
  public var game: GameMode;
  public var animList: Array<Animation>;
  public var bgList: Array<SpecialBg>;
  public var mcList: Array<MovieClip>;
  public var stack: Array<DelayedFx>;
  public var fl_bg: Bool;
  public var levelName: DynamicMovieClip /* TODO */;
  public var nameTimer: Float;
  public var lastAlert: MovieClip;
  public var mc_exitArrow: MovieClip;
  public var igMsg: DynamicMovieClip /* TODO */;

  public function new(g: GameMode): Void;

  public function attachLevelPop(name: String, fl_label: Bool): Void;

  public function attachAlert(str: String): MovieClip;

  public function detachLastAlert(): Void;

  public function attachHurryUp(): MovieClip;

  public function attachWarning(): MovieClip;

  public function attachExit(): Void;

  public function detachExit(): Void;

  public function attachEnter(x: Float, pid: Int): Void;

  public function attachScorePop(color: Int, glowColor: Int, x: Float, y: Float, txt: String): Void;

  public function attachExplodeZone(x: Float, y: Float, radius: Float): Animation;

  public function attachExplosion(x: Float, y: Float, radius: Float): Animation;

  public function attachShine(x: Float, y: Float): Animation;

  public function keyRequired(kid: Int): Void;

  public function keyUsed(kid: Int): Void;

  public function attachFx(x: Float, y: Float, link: String): Animation;

  public function dust(cx: Int, cy: Int): Void;

  public function delayFx(t: Float, x: Float, y: Float, link: String): Void;

  public function inGameParticles(id: Int, x: Float, y: Float, n: Int): Void;

  public function inGameParticlesDir(id: Int, x: Float, y: Float, n: Int, dir: Null<Float>): Void;

  public function attachBg(id: Int, subId: Int, timer: Float): Void;

  public function detachBg(): Void;

  public function clearBg(): Void;

  public function clear(): Void;

  public function onNextLevel(): Void;

  public static function addGlow(mc: MovieClip, color: Int, length: Float): Void;

  public function main(): Void;
}
