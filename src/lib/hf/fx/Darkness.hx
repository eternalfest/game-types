package hf.fx;

import etwin.flash.MovieClip;

/**
 * Class representing the `hammer_fx_darkness` sprite.
 */
@:interface
extern class Darkness extends MovieClip {
  public var hole1: MovieClip;
  public var hole2: MovieClip;
  public var holes: Array<MovieClip>;
}
