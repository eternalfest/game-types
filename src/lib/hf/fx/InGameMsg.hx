package hf.fx;

import etwin.flash.MovieClip;
import etwin.flash.TextField;

@:interface
extern class InGameMsg extends MovieClip {
  public var field: TextField;
  public var label: TextField;
}
