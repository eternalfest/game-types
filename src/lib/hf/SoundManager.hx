package hf;

import etwin.flash.MovieClip;
import etwin.flash.Sound;

typedef SoundChannel = {
  var mc: MovieClip;
  var sounds: Hash;
  var vol: Float;
  var vol_ctrl: Sound;
  var nb: Int;
  var enabled: Bool;
}

extern class SoundManager {
  public var root_mc: MovieClip;
  public var depth: Int;
  public var channels: Array<SoundChannel>;
  public var flag_playing: Null<Bool>;
  public var fade_from: SoundChannel;
  public var fade_start: Float;
  public var fade_to: SoundChannel;
  public var fade_end: Float;
  public var last_time: Int;
  public var fade_pos: Float;
  public var fade_len: Float;

  public function new(mc: MovieClip, base_depth: Int): Void;

  public static function do_stop_sound(k: String, s: Sound): Void;

  public function on_sound_completed(): Void;

  public function destroy(): Void;

  public function getChannel(chan: Int): SoundChannel;

  public function getSound(name: String, chan: Int): Sound;

  public function playSound(name: String, chan: Int): Void;

  public function play(name: String): Void;

  public function loop(name: String, chan: Int): Void;

  public function stopSound(name: String, chan: Int): Void;

  public function fade(chan_from: Int, chan_to: Int, length: Float): Void;

  public function main(): Void;

  public function enable(chan: Int, flag: Bool): Void;

  public function stop(chan: Int): Void;

  public function isPlaying(name: String, chan: Int): Bool;

  public function setVolume(chan: Int, volume: Float): Void;
}
