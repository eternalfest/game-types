package hf;

extern class Stat {
  public var current: Int;
  public var total: Int;

  public function new(): Void;

  public function inc(n: Int): Void;

  public function reset(): Void;
}
