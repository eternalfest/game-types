package hf;

/**
 * Class providing global time measurement that can be accessed from various parts of the engine.
 *
 * The values are smoothed across frames to improve support for browser/flash platforms where FPS
 * can't be fixed using `vsync`. This reduces the sensibility to sudden spikes, for example when
 * spawning many particles.
 *
 * @see https://github.com/HeapsIO/heaps/blob/master/hxd/Timer.hx
 *
 * The Timer class acts as a global time measurement that can be accessed from various parts of the engine.
 * These three values are representation of the same underlying calculus: tmod, dt, fps
 */
extern class Timer {
  /**
   * The FPS on which `tmod` values are based on.
   *
   * Default: `32`
   * @final
   */
  public static var wantedFPS: Float;

  /**
   * Maximum duration between two frames (in seconds).
   *
   * If the duration exceeds this amount, the timer will not treat it as a lag
   * but as if the engine itself was frozen. A non-lag cause for this may be
   * when the computer moves to sleep and supsends all its active programs.
   *
   * Default: `0.5`
   * @final
   */
  public static var maxDeltaTime: Float;

  /**
   * The last timestamp when `Timer.update` was called.
   */
  public static var oldTime: Int;

  /**
   * The smoothing done between frames.
   *
   * A smoothing of 0 gives "real time" values, higher values will smooth the results for `tmod`
   * over frames using the following formula:
   *
   * ```
   * tmod = lerp(tmod, deltaT, tmod_factor)
   * ```
   *
   * Default: `0.95`
   * @final
   */
  public static var tmod_factor: Float;

  /**
   * Internal temporary value used to compute frame smoothing.
   */
  public static var calc_tmod: Float;

  /**
   * Smoothed duration between the current and previous frames (in seconds).
   */
  public static var tmod: Float;

  /**
   * Real duration between the current and previous frames (in seconds).
   */
  public static var deltaT: Float;

  /**
   * A frame counter, increases on each call to `Timer.update`.
   */
  public static var frameCount: Int;

  /**
   * Update the timer calculus on each frame.
   *
   * It is automatically called by `GameManager.main`
   */
  public static function update(): Void;

  /**
   * Returns the current smoothed FPS.
   */
  public static function fps(): Float;
}
