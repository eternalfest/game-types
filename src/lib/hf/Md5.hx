package hf;

extern class Md5 {
  public static var hex_chr: String;

  public function new(): Void;

  public static function bitOR(a: Int, b: Int): Int;

  public static function bitXOR(a: Int, b: Int): Int;

  public static function bitAND(a: Int, b: Int): Int;

  public static function addme(x: Int, y: Int): Int;

  public static function rhex(num: Int): String;

  public static function str2blks(str: String): Array<Int>;

  public static function rol(num: Int, cnt: Int): Int;

  public static function cmn(q: Int, a: Int, b: Int, x: Int, s: Int, t: Int): Int;

  public static function ff(a: Int, b: Int, c: Int, d: Int, x: Int, s: Int, t: Int): Int;

  public static function gg(a: Int, b: Int, c: Int, d: Int, x: Int, s: Int, t: Int): Int;

  public static function hh(a: Int, b: Int, c: Int, d: Int, x: Int, s: Int, t: Int): Int;

  public static function ii(a: Int, b: Int, c: Int, d: Int, x: Int, s: Int, t: Int): Int;

  public static function encode(str: String): String;
}
