package hf.native;

import etwin.flash.display.BitmapData in NativeBitmapData;
import etwin.flash.MovieClip;

@:native("flash.display.BitmapData")
@:interface
extern class BitmapData extends NativeBitmapData {
  function drawMC(mc: MovieClip, x: Float, y: Float): Void;
}
