package hf;

import etwin.flash.MovieClip;
import hf.Delta;
import hf.Pos;

extern class Tools {
  /**
   * The raw string `"x"`.
   *
   * Initial value: `Obfu.raw("x")`
   */
  public static var __x: String;

  /**
   * The raw string `"y"`.
   *
   * Initial value: `Obfu.raw("y")`
   */
  public static var __y: String;

  /**
   * List of raw strings used internally by `Tools`.
   *
   * It is declared with a `null` value and must be initialized by calling
   * `Tools.initStrings`.
   */
  public static var strings: Null<Array<String>>;

  /**
   * Schedules a timeout to call `f` after `time` milliseconds.
   */
  public static function delay(f: Void -> Void, time: Float): Void;

  /**
   * Finds all the occurences of `search` in `str` and replaces them with `replace`.
   *
   * This function first finds all the occurences to replace, and only then applies the
   * replacements.
   * If `str` is `null`, returns `null`.
   * `search` MUST be non-empty.
   */
  public static function replace(str: Null<String>, search: String, replace: String): Null<String>;

  /**
   * Splits `str` into an array of lines.
   *
   * The lines are split at the end of lines: `\r?\n`.
   * The strings in the result array do not include the end of line.
   * The result array always has at least one line.
   *
   * Example:
   * ```
   * Tools.lines(""); // [""]
   * Tools.lines("a\nb"); // ["a", "b"]
   * Tools.lines("a\r\nb"); // ["a", "b"]
   * Tools.lines("a\n\nb"); // ["a", "", "b"]
   * Tools.lines("a\nb\n"); // ["a", "b", ""]
   * ```
   */
  public static function lines(str: String): Array<String>;

  /**
   * Shallow-clones `_a` and shuffles its items.
   */
  // TODO: Generic
  // public static function shuffle<T>(_a: Array<T>): Array<T>;
  public static function shuffle(_a: Array<Dynamic>): Array<Dynamic>;

  public static function rootDelta(mc: MovieClip): Delta;

  /**
   * Stringifies `x` with at least `ndigits` digits, padding with zeros if needed.
   *
   * ```
   * Tools.padZeros(3, 0); // "3"
   * Tools.padZeros(3, 1); // "3"
   * Tools.padZeros(3, 2); // "03"
   * Tools.padZeros(15, 1); // "15"
   * Tools.padZeros(15, 3); // "015"
   * Tools.padZeros(15, 6); // "000015"
   * ```
   */
  public static function padZeros(x: Int, ndigits: Int): String;

  /**
   * Picks an index of `a` weighted with the corresponding value.
   *
   * Example:
   * ```
   * Tools.randomProbas([20, 0, 10, 10]);
   * // Possible return values:
   * // - `0` (50% chance)
   * // - `1` has a 0% chance so it won't be returned.
   * // - `2` (25% chance)
   * // - `3` (25% chance)
   * ```
   */
  public static function randomProbas(a: Array<Int>): Int;

  /**
   * Returns the distance between the centers of the bounds of the provided movie clips.
   *
   * Both movie clips must use the same coordinate space, otherwise the result is unspecified.
   */
  public static function distMC(mc1: MovieClip, mc2: MovieClip): Float;

  /**
   * Returns the child movie clips of `mc`.
   */
  public static function subs(mc: MovieClip): Array<MovieClip>;

  public static function localToGlobal(mc: MovieClip, x: Float, y: Float): Pos<Float>;

  public static function globalToLocal(mc: MovieClip, x: Float, y: Float): Pos<Float>;

  /**
   * Initializes `Tools.strings` with a list of raw strings.
   *
   * After the initialization, `Tools.strings` has the following value:
   * ```
   * ["linear", "radial", "matrixType", "box", "x", "y", "w", "h", "r"]
   * ```
   */
  public static function initStrings(): Void;

  public static function beginGradientFill(mc: MovieClip, linear: Bool, colors: Dynamic /*TODO*/, alphas: Dynamic /*TODO*/, coefs: Dynamic /*TODO*/, matrix: Dynamic /*TODO*/): Void;
}
