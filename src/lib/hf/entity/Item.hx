package hf.entity;

import hf.entity.Physics;
import hf.entity.Player;
import hf.mode.GameMode;

extern class Item extends Physics {
  public var id: Int;
  public var subId: Int;

  public function new(): Void;

  public function initItem(g: GameMode, x: Float, y: Float, id: Int, subId: Null<Int>): Void;

  public function execute(p: Player): Void;

  override public function init(g: GameMode): Void;

  override public function onDeathLine(): Void;

  override public function onLifeTimer(): Void;

  override public function update(): Void;
}
