package hf.entity.boss;

import etwin.flash.filters.GlowFilter;
import hf.entity.Mover;
import hf.entity.shoot.BossFireBall;
import hf.mode.GameMode;

extern class Bat extends Mover {
  public static var GLOW_BASE: Float;
  public static var GLOW_RANGE: Float;
  public static var RADIUS: Float;
  public static var SPEED: Float;
  public static var SHOOT_SPEED: Float;
  public static var LIVES: Int;
  public static var WAIT_TIME: Float;
  public static var FLOAT_X: Float;
  public static var FLOAT_Y: Float;
  public static var MAX_FALL_ROTATION: Float;

  public var fl_trap: Bool;
  public var fl_move: Bool;
  public var fl_wait: Bool;
  public var fl_shield: Bool;
  public var fl_immune: Bool;
  public var immuneTimer: Float;
  public var fl_anger: Bool;
  public var floatOffset: Float;
  public var fl_death: Bool;
  public var fl_deathUp: Bool;
  public var lives: Int;
  public var fbList: Array<BossFireBall>;
  public var glow: GlowFilter;
  public var tx: Float;
  public var ty: Float;
  public var glowCpt: Float;

  public function new(): Void;

  public static function attach(g: GameMode): Bat;

  public function initBoss(g: GameMode): Void;

  public function moveRandom(): Void;

  public function flip(): Void;

  public function halt(): Void;

  public function immune(): Void;

  public function removeImmunity(): Void;

  public function wait(): Void;

  public function stopWait(): Void;

  public function shield(): Void;

  public function removeShield(): Void;

  public function freeze(d: Float): Void;

  public function knock(): Void;

  public function onPlayerDeath(): Void;

  public function loseLife(): Void;

  public function kill(): Void;

  public function attachFireBall(ang: Float, distFactor: Float): BossFireBall;

  public function bossAnger(): Void;

  public function bossCalmDown(): Void;

  override public function destroy(): Void;

  override public function endUpdate(): Void;

  override public function infix(): Void;

  override public function init(g: GameMode): Void;

  override public function killHit(dx: Null<Float>): Void;

  override public function onEndAnim(id: Int): Void;

  override public function onNext(): Void;

  override public function prefix(): Void;

  override public function update(): Void;
}
