package hf.entity.boss;

import etwin.flash.DynamicMovieClip;
import hf.entity.Item;
import hf.entity.Mover;
import hf.Entity;
import hf.mode.GameMode;

extern class Tuberculoz extends Mover {
  public static var auto_inc: Int;
  public static var WALK: Int;
  public static var JUMP: Int;
  public static var DASH: Int;
  public static var BOMB: Int;
  public static var HIT: Int;
  public static var BURN: Int;
  public static var TORNADO: Int;
  public static var TORNADO_END: Int;
  public static var DIE: Int;
  public static var seq_inc: Int;
  public static var SEQ_BURN: Int;
  public static var SEQ_TORNADO: Int;
  public static var SEQ_DASH: Int;
  public static var LAST_SEQ: Int;
  public static var SEQ_DURATION: Float;
  public static var LIVES: Int;
  public static var GROUND_Y: Float;
  public static var CENTER_OFFSET: Float;
  public static var HEAD_OFFSET_X: Float;
  public static var HEAD_OFFSET_Y: Float;
  public static var RADIUS: Float;
  public static var HEAD_RADIUS: Float;
  public static var MAX_BADS: Int;
  public static var INVERT_KICK_X: Float;
  public static var WALK_SPEED: Float;
  public static var DASH_SPEED: Float;
  public static var FIREBALL_SPEED: Float;
  public static var TORNADO_INTRO: Float;
  public static var TORNADO_DURATION: Float;
  public static var JUMP_Y: Float;
  public static var JUMP_EST_X: Float;
  public static var JUMP_EST_Y: Float;
  public static var A_STEP: Int;
  public static var B_STEP: Int;
  public static var EXTRA_LIFE_STEP: Int;
  public static var CHANCE_PLAYER_JUMP: Int;
  public static var CHANCE_BOMB_JUMP: Int;
  public static var CHANCE_DASH: Int;
  public static var CHANCE_SPAWN: Int;
  public static var CHANCE_BURN: Int;
  public static var CHANCE_FINAL_ANGER: Int;

  public var _firstUniq: Int;
  public var action: Int;
  public var fl_trap: Bool;
  public var fl_shield: Bool;
  public var fl_immune: Bool;
  public var immuneTimer: Float;
  public var recents: Array<Bool>;
  public var fl_death: Bool;
  public var fl_defeated: Bool;
  public var fbCoolDown: Float;
  public var seqTimer: Float;
  public var defeatTimeOut: Float;
  public var lives: Int;
  public var badKills: Int;
  public var totalKills: Int;
  public var seq: Int;
  public var lifeBar: DynamicMovieClip /* TODO */;
  public var fl_twister: Bool;
  public var dashCount: Null<Int>;
  public var itemA: Null<Item>;
  public var itemB: Null<Item>;

  public function new(): Void;

  public static function attach(g: GameMode): Tuberculoz;

  public function initBoss(g: GameMode): Void;

  public function getHitList(t: Int): Array<Entity>;

  public function flag(uid: Int): Void;

  public function checkFlag(uid: Int): Bool;

  public function updateBar(): Void;

  public function loseLife(n: Int): Void;

  public function immune(): Void;

  public function spawnBombs(n: Int): Void;

  public function kickBomb(b: Bomb): Void;

  public function isWindCompatible(e: Entity): Bool;

  public function updateDash(): Void;

  public function updateTornado(): Void;

  public function updateDeath(): Void;

  public function ia(): Void;

  public function halt(): Void;

  public function walk(): Void;

  public function jump(jumpY: Float): Void;

  public function land(): Void;

  public function dash(): Void;

  public function dropBombs(): Void;

  public function burn(): Void;

  public function tornado(): Void;

  public function die(): Void;

  /**
   * Normalized to `final` during obfuscation.
   */
  public function _final(): Void;

  public function openExit(): Void;

  public function onPlayerDeath(): Void;

  public function onKillBad(): Void;

  public function onExplode(x: Float, y: Float, radius: Float): Void;

  override public function checkHits(): Void;

  override public function destroy(): Void;

  override public function endUpdate(): Void;

  override public function init(g: GameMode): Void;

  override public function onEndAnim(id: Int): Void;

  override public function onNext(): Void;

  override public function update(): Void;
}
