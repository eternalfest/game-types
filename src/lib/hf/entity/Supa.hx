package hf.entity;

import hf.entity.Mover;
import hf.mode.GameMode;

extern class Supa extends Mover {
  public var speed: Float;
  public var radius: Float;

  public function new(): Void;

  public function initSupa(g: GameMode, x: Float, y: Float): Void;

  override public function infix(): Void;

  override public function init(g: GameMode): Void;

  override public function tAdd(cx: Int, cy: Int): Void;

  override public function tRem(cx: Int, cy: Int): Void;
}
