package hf.entity;

import hf.Entity;
import hf.Pos;
import hf.entity.Animator;
import hf.levels.TeleporterData;
import hf.mode.GameMode;

typedef MvtSteps = {
  var total: Float;
  var dx: Float;
  var dy: Float;
}

extern class Physics extends Animator {
  public var gravityFactor: Float;
  public var fallFactor: Float;
  public var shockResistance: Float;
  public var fl_stable: Bool;
  public var fl_physics: Bool;
  public var fl_friction: Bool;
  public var fl_gravity: Bool;
  public var fl_strictGravity: Bool;
  public var fl_hitGround: Bool;
  public var fl_hitCeil: Bool;
  public var fl_hitWall: Bool;
  public var fl_hitBorder: Bool;
  public var fl_slide: Bool;
  public var fl_teleport: Bool;
  public var fl_portal: Bool;
  public var fl_bump: Bool;
  public var fl_moveable: Bool;
  public var fl_wind: Bool;
  public var fl_skipNextGravity: Bool;
  public var fl_skipNextGround: Bool;
  public var fl_stopStepping: Bool;
  public var lastTeleporter: TeleporterData;
  public var fallStart: Float;
  public var slideFriction: Float;

  // Used by entity.bad.Flyer and entity.bad.WallWalker
  public var fl_intercept: Null<Bool>;

  public function new(): Void;

  public function enablePhysics(): Void;

  public function disablePhysics(): Void;

  public function shockWave(e: Physics, radius: Float, power: Float): Void;

  public function killHit(dx: Null<Float>): Void;

  public function resurrect(): Void;

  public function moveToAng(angDeg: Float, speed: Float): Void;

  public function moveToTarget(e: Pos<Float>, speed: Float): Void;

  public function moveToPoint(x: Float, y: Float, speed: Float): Void;

  public function moveUp(speed: Float): Void;

  public function moveDown(speed: Float): Void;

  public function moveLeft(speed: Float): Void;

  public function moveRight(speed: Float): Void;

  public function moveFrom(e: Pos<Float>, speed: Float): Void;

  public function moveToGround(): Void;

  public function checkHits(): Void;

  public function prefix(): Void;

  public function infix(): Void;

  public function postfix(): Void;

  public function recal(): Void;

  public function calcSteps(dxStep: Float, dyStep: Float): MvtSteps;

  public function needsPatch(): Bool;

  public function onKill(): Void;

  public function onDeathLine(): Void;

  public function onHitWall(): Void;

  public function onHitGround(f: Float): Void;

  public function onHitCeil(): Void;

  public function onTeleport(): Void;

  public function onPortal(pid: Int): Void;

  public function onPortalRefusal(): Void;

  public function onBump(): Void;

  override public function endUpdate(): Void;

  override public function init(g: GameMode): Void;

  override public function update(): Void;
}
