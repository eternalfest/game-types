package hf.entity;

import hf.Entity;

extern class Trigger extends Entity {
  public var fl_largeTrigger: Bool;

  public function new(): Void;

  public function tAddSingle(cx: Int, cy: Int): Void;

  public function tRemSingle(cx: Int, cy: Int): Void;

  public function tAdd(cx: Int, cy: Int): Void;

  public function tRem(cx: Int, cy: Int): Void;

  public function moveToCase(cx: Float, cy: Float): Void;

  public function getByType(type: Int): Array<Entity>;

  override public function moveTo(x: Float, y: Float): Void;
}
