package hf.entity;

import hf.entity.Physics;
import hf.entity.Player;
import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

extern class WalkingBomb extends Physics {
  public var left: Int;
  public var right: Int;
  public var fl_unstable: Bool;
  public var fl_knock: Bool;
  public var knockTimer: Float;
  public var realBomb: PlayerBomb;

  public function new(): Void;

  public static function attach(g: GameMode, b: PlayerBomb): WalkingBomb;

  public function initBomb(g: GameMode, b: PlayerBomb): Void;

  public function destroyBoth(): Void;

  public function onKick(p: Player): Void;

  public function onExplode(): Void;

  public function getControls(): Void;

  public function knock(d: Float): Void;

  public function checkClimb(): Void;

  public function jump(jx: Float, jy: Float): Void;

  override public function endUpdate(): Void;

  override public function hit(e: Entity): Void;

  override public function infix(): Void;

  override public function onDeathLine(): Void;

  override public function onHitGround(h: Float): Void;

  override public function onHitWall(): Void;

  override public function onLifeTimer(): Void;

  override public function onPortal(pid: Int): Void;

  override public function onPortalRefusal(): Void;

  override public function needsPatch(): Bool;

  override public function update(): Void;
}
