package hf.entity.bad.walker;

import hf.entity.bad.Jumper;
import hf.mode.GameMode;

extern class Ananas extends Jumper {
  public static var CHANCE_DASH: Int;

  public var dashRadius: Float;
  public var dashPower: Float;
  public var fl_attack: Bool;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Ananas;

  public function repel(type: Int, powerFactor: Float): Void;

  public function vaporize(type: Int): Void;

  public function startAttack(): Void;

  public function attack(): Void;

  override public function freeze(d: Float): Void;

  override public function init(g: GameMode): Void;

  override public function isReady(): Bool;

  override public function killHit(dx: Null<Float>): Void;

  override public function knock(d: Float): Void;

  override public function onEndAnim(id: Int): Void;

  override public function onHitGround(h: Float): Void;

  override public function onHitWall(): Void;

  override public function prefix(): Void;
}
