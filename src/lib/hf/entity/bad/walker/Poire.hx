package hf.entity.bad.walker;

import hf.entity.bad.Shooter;
import hf.mode.GameMode;

extern class Poire extends Shooter {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Poire;

  override public function onShoot(): Void;
}
