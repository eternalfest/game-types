package hf.entity.bad.walker;

import hf.entity.bad.Jumper;
import hf.mode.GameMode;

extern class LitchiWeak extends Jumper {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): LitchiWeak;

  override public function onEndAnim(id: Int): Void;

  override public function walk(): Void;
}
