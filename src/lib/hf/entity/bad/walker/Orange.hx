package hf.entity.bad.walker;

import hf.entity.bad.Jumper;
import hf.mode.GameMode;

extern class Orange extends Jumper {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Orange;
}
