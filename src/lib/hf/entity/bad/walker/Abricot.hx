package hf.entity.bad.walker;

import hf.entity.bad.Jumper;
import hf.mode.GameMode;

extern class Abricot extends Jumper {
  public var fl_spawner: Bool;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float, spawner: Bool): Abricot;

  override public function init(g: GameMode): Void;

  override public function onDeathLine(): Void;
}
