package hf.entity.bad.walker;

import hf.entity.bad.Jumper;
import hf.entity.Animator.Anim;
import hf.mode.GameMode;

extern class Bombe extends Jumper {
  public static var RADIUS: Float;
  public static var EXPERT_RADIUS: Float;
  public static var POWER: Float;

  public var fl_overheat: Bool;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Bombe;

  public function trigger(): Void;

  public function selfDestruct(): Void;

  override public function init(g: GameMode): Void;

  override public function isHealthy(): Bool;

  override public function killHit(dx: Null<Float>): Void;

  override public function onFreeze(): Void;

  override public function onLifeTimer(): Void;

  override public function playAnim(obj: Anim): Void;

  override public function prefix(): Void;
}
