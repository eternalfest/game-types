package hf.entity.bad.walker;

import hf.entity.bad.Shooter;
import hf.mode.GameMode;

extern class Pomme extends Shooter {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Pomme;

  override public function init(g: GameMode): Void;

  override public function onShoot(): Void;
}
