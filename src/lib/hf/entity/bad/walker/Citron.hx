package hf.entity.bad.walker;

import hf.entity.bad.Shooter;
import hf.mode.GameMode;

extern class Citron extends Shooter {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Citron;

  override public function onShoot(): Void;
}
