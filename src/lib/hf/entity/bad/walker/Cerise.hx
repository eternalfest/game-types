package hf.entity.bad.walker;

import hf.entity.bad.Walker;
import hf.mode.GameMode;

extern class Cerise extends Walker {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Cerise;
}
