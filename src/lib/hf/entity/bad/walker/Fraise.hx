package hf.entity.bad.walker;

import hf.entity.bad.Shooter;
import hf.entity.shoot.Ball;
import hf.mode.GameMode;

extern class Fraise extends Shooter {
  public var fl_ball: Bool;
  public var catchCD: Float;
  public var ballTarget: Null<Entity>;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Fraise;

  public function catchBall(b: Ball): Void;

  public function assignBall(): Void;

  override public function destroy(): Void;

  override public function endUpdate(): Void;

  override public function init(g: GameMode): Void;

  override public function onEndAnim(id: Int): Void;

  override public function onShoot(): Void;

  override public function startShoot(): Void;

  override public function update(): Void;
}
