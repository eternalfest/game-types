package hf.entity.bad.walker;

import hf.entity.bad.Jumper;
import hf.entity.bad.walker.LitchiWeak;
import hf.mode.GameMode;

extern class Litchi extends Jumper {
  public var child: Null<LitchiWeak>;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Litchi;

  public function weaken(): Void;

  override public function endUpdate(): Void;

  override public function forceKill(dx: Null<Float>): Void;

  override public function freeze(d: Float): Void;

  override public function killHit(dx: Null<Float>): Void;

  override public function update(): Void;
}
