package hf.entity.bad.walker;

import hf.entity.bad.Shooter;
import hf.entity.bomb.bad.Mine;
import hf.mode.GameMode;

extern class Kiwi extends Shooter {
  public var mineList: Array<Mine>;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Kiwi;

  public function clearMines(): Void;

  override public function onShoot(): Void;
}
