package hf.entity.bad.walker;

import hf.entity.bad.Shooter;
import hf.entity.shoot.FramBall;
import hf.mode.GameMode;

extern class Framboise extends Shooter {
  public static var FRAGS: Int;
  public static var MAX_TRIES: Int;

  public var white: Float;
  public var fl_phased: Bool;
  public var tx: Null<Float>;
  public var ty: Null<Float>;
  public var arrived: Null<Int>;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Framboise;

  public function aroundX(base: Float): Float;

  public function aroundY(base: Float): Float;

  public function onArrived(fb: FramBall): Void;

  public function clearFrags(): Void;

  public function phaseOut(): Void;

  public function phaseIn(): Void;

  override public function destroy(): Void;

  override public function endUpdate(): Void;

  override public function freeze(d: Float): Void;

  override public function init(g: GameMode): Void;

  override public function isReady(): Bool;

  override public function killHit(dx: Null<Float>): Void;

  override public function knock(d: Float): Void;

  override public function onHitGround(h: Float): Void;

  override public function onShoot(): Void;

  override public function update(): Void;

  override public function walk(): Void;
}
