package hf.entity.bad;

import hf.entity.bad.Jumper;
import hf.mode.GameMode;

extern class Shooter extends Jumper {
  public var shootCD: Float;
  public var shootPreparation: Float;
  public var shootDuration: Float;
  public var fl_shooter: Bool;
  public var chanceShoot: Float;

  public function new(): Void;

  public function initShooter(prepa: Float, duration: Float): Void;

  public function enableShooter(): Void;

  public function disableShooter(): Void;

  public function setShoot(chance: Null<Float>): Void;

  public function startShoot(): Void;

  public function onShoot(): Void;

  override public function init(g: GameMode): Void;

  override public function onEndAnim(id: Int): Void;

  override public function onNext(): Void;

  override public function update(): Void;
}
