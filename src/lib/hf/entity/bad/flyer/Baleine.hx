package hf.entity.bad.flyer;

import hf.entity.bad.Flyer;
import hf.mode.GameMode;

/**
 * https://i.imgur.com/lzb51Dn.jpg
 */
extern class Baleine extends Flyer {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Baleine;
}
