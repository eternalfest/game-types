package hf.entity.bad.flyer;

import hf.entity.bad.Flyer;
import hf.Entity;
import hf.mode.GameMode;

extern class Tzongre extends Flyer {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Tzongre;

  override public function freeze(d: Float): Void;

  override public function hit(e: Entity): Void;

  override public function init(g: GameMode): Void;

  override public function killHit(dx: Null<Float>): Void;

  override public function knock(d: Float): Void;
}
