package hf.entity.bad;

import hf.entity.Bad;
import hf.Entity;
import hf.mode.GameMode;

extern class Spear extends Bad {
  public var skin: Int;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Spear;

  override public function burn(): Void;

  override public function freeze(d: Float): Void;

  override public function hit(e: Entity): Void;

  override public function initBad(g: GameMode, x: Float, y: Float): Void;

  override public function init(g: GameMode): Void;

  override public function killHit(dx: Null<Float>): Void;

  override public function knock(d: Float): Void;
}
