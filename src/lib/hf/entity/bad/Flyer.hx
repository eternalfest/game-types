package hf.entity.bad;

import hf.entity.Bad;
import hf.entity.Animator.Anim;
import hf.mode.GameMode;

extern class Flyer extends Bad {
  public var speed: Float;
  public var fl_fly: Bool;
  public var xSpeed: Float;
  public var ySpeed: Float;

  public function new(): Void;

  public function fly(): Void;

  public function land(): Void;

  override public function endUpdate(): Void;

  override public function infix(): Void;

  override public function init(g: GameMode): Void;

  override public function killHit(dx: Null<Float>): Void;

  override public function onFreeze(): Void;

  override public function onHitCeil(): Void;

  override public function onHitGround(h: Float): Void;

  override public function onHitWall(): Void;

  override public function onKnock(): Void;

  override public function onMelt(): Void;

  override public function onNext(): Void;

  override public function onWakeUp(): Void;

  override public function playAnim(a: Anim): Void;

  override public function postfix(): Void;

  override public function update(): Void;

  override public function updateSpeed(): Void;
}
