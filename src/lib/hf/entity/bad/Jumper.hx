package hf.entity.bad;

import hf.entity.bad.Walker;
import hf.mode.GameMode;

extern class Jumper extends Walker {
  public var fl_jUp: Bool;
  public var chanceJumpUp: Float;
  public var fl_jDown: Bool;
  public var chanceJumpDown: Float;
  public var fl_jH: Bool;
  public var chanceJumpH: Float;
  public var fl_climb: Bool;
  public var chanceClimb: Float;
  public var maxClimb: Int;
  public var fl_jumper: Bool;
  public var jumpTimer: Float;

  public function new(): Void;

  public function setJumpUp(chance: Null<Float>): Void;

  public function setJumpDown(chance: Null<Float>): Void;

  public function setJumpH(chance: Null<Float>): Void;

  public function setClimb(chance: Null<Float>, max: Null<Int>): Void;

  public function setJumper(): Void;

  public function decideJumpUp(): Bool;

  public function decideJumpDown(): Bool;

  public function decideClimb(): Bool;

  public function jump(dx: Float, dy: Float, delay: Float): Void;

  public function checkClimb(): Void;

  override public function infix(): Void;

  override public function init(g: GameMode): Void;

  override public function onFall(): Void;

  override public function onFreeze(): Void;

  override public function onHitWall(): Void;

  override public function onKnock(): Void;

  override public function onNext(): Void;
}
