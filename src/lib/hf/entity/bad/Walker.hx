package hf.entity.bad;

import hf.entity.Bad;
import hf.mode.GameMode;

extern class Walker extends Bad {
  public var speed: Float;
  public var fl_willFallDown: Bool;
  public var recentParticles: Float;
  public var fl_fall: Bool;
  public var chanceFall: Float;

  public function new(): Void;

  public function halt(): Void;

  public function walk(): Void;

  public function fallBack(): Void;

  public function setFall(chance: Float): Void;

  public function decideFall(): Bool;

  public function onFall(): Void;

  override public function angerMore(): Void;

  override public function calmDown(): Void;

  override public function endUpdate(): Void;

  override public function infix(): Void;

  override public function init(g: GameMode): Void;

  override public function isReady(): Bool;

  override public function onFreeze(): Void;

  override public function onHitGround(h: Float): Void;

  override public function onHitWall(): Void;

  override public function onKnock(): Void;

  override public function onMelt(): Void;

  override public function onNext(): Void;

  override public function onWakeUp(): Void;

  override public function postfix(): Void;

  override public function update(): Void;

  override public function updateSpeed(): Void;
}
