package hf.entity.bad;

import etwin.flash.MovieClip;
import hf.entity.Bad;
import hf.entity.Player;
import hf.entity.Animator.Anim;
import hf.Entity;
import hf.mode.GameMode;

extern class FireBall extends Bad {
  public var speed: Float;
  public var angSpeed: Float;
  public var ang: Float;
  public var summonTimer: Float;
  public var fl_summon: Bool;
  public var angerTimer: Float;
  public var body: MovieClip;
  public var eyes: MovieClip;
  public var tang: Float;

  public function new(): Void;

  public static function attach(g: GameMode, p: Player): FireBall;

  override public function angerMore(): Void;

  override public function endUpdate(): Void;

  override public function freeze(d: Float): Void;

  override public function hit(e: Entity): Void;

  override public function init(g: GameMode): Void;

  override public function killHit(dx: Null<Float>): Void;

  override public function knock(d: Float): Void;

  override public function onDeathLine(): Void;

  override public function onHurryUp(): Void;

  override public function playAnim(o: Anim): Void;

  override public function tAdd(cx: Int, cy: Int): Void;

  override public function tRem(cx: Int, cy: Int): Void;

  override public function update(): Void;
}
