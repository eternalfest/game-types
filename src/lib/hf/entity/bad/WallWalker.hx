package hf.entity.bad;

import hf.entity.Bad;
import hf.mode.GameMode;
import hf.Pos;

typedef MvtParams = {
  var x: Float;
  var y: Float;
  var cp: Pos<Int>;
  var xSpeed: Float;
  var ySpeed: Float;
}

extern class WallWalker extends Bad {
  public static var SUB_RECAL: Float;

  public var speed: Float;
  public var subOffset: Float;
  public var fl_lost: Bool;
  public var xSub: Float;
  public var xSubBase: Float;
  public var ySub: Float;
  public var ySubBase: Float;
  public var fl_wallWalk: Null<Bool>;
  public var xSpeed: Float;
  public var ySpeed: Float;
  public var cp: Pos<Int>;
  public var lastSafe: Null<MvtParams>;

  public function new(): Void;

  public function wallWalk(): Void;

  public function land(): Void;

  // xoff: -1 | 0 | 1
  // yoff: -1 | 0 | 1
  public function setDir(xoff: Int, yoff: Int): Void;

  // xoff: -1 | 0 | 1
  // yoff: -1 | 0 | 1
  public function setCP(xoff: Int, yoff: Int): Void;

  public function wallWalkIA(): Void;

  public function suicide(): Void;

  public function moveToSafePos(): Void;

  override public function endUpdate(): Void;

  override public function infix(): Void;

  override public function initBad(g: GameMode, x: Float, y: Float): Void;

  override public function isReady(): Bool;

  override public function killHit(dx: Null<Float>): Void;

  override public function onFreeze(): Void;

  override public function onHitGround(h: Float): Void;

  override public function onHitWall(): Void;

  override public function onKnock(): Void;

  override public function onMelt(): Void;

  override public function onWakeUp(): Void;

  override public function update(): Void;

  override public function updateSpeed(): Void;
}
