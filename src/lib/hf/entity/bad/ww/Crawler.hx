package hf.entity.bad.ww;

import hf.entity.bad.WallWalker;
import hf.mode.GameMode;

extern class Crawler extends WallWalker {
  public static var SCALE_RECAL: Float;
  public static var CRAWL_STRETCH: Float;
  public static var COLOR: Int;
  public static var COLOR_ALPHA: Float;
  public static var SHOOT_SPEED: Float;
  public static var CHANCE_ATTACK: Int;
  public static var COOLDOWN: Float;
  public static var ATTACK_TIMER: Float;

  public var fl_attack: Bool;
  public var _tolerates: Float;
  public var attackCD: Float;
  public var attackTimer: Float;
  public var colorAlpha: Null<Float>;
  public var xscale: Float;
  public var yscale: Float;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Crawler;

  public function prepareAttack(): Void;

  public function attack(): Void;

  public function decideAttack(): Bool;

  override public function endUpdate(): Void;

  override public function initBad(g: GameMode, x: Float, y: Float): Void;

  override public function isReady(): Bool;

  override public function killHit(dx: Null<Float>): Void;

  override public function onEndAnim(id: Int): Void;

  override public function onFreeze(): Void;

  override public function onHitGround(h: Float): Void;

  override public function onKnock(): Void;

  override public function update(): Void;
}
