package hf.entity.bad.ww;

import hf.entity.bad.WallWalker;
import hf.Entity;
import hf.mode.GameMode;

extern class Saw extends WallWalker {
  public static var ROTATION_RECAL: Float;
  public static var STUN_DURATION: Float;
  public static var BASE_SPEED: Float;
  public static var ROTATION_SPEED: Float;

  public var rotSpeed: Float;
  public var fl_stun: Bool;
  public var fl_stop: Bool;
  public var fl_updateSpeed: Bool;
  public var stunTimer: Float;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Saw;

  public function stun(): Void;

  public function halt(): Void;

  public function run(): Void;

  override public function endUpdate(): Void;

  override public function freeze(d: Float): Void;

  override public function hit(e: Entity): Void;

  override public function initBad(g: GameMode, x: Float, y: Float): Void;

  override public function init(g: GameMode): Void;

  override public function isHealthy(): Bool;

  override public function killHit(dx: Null<Float>): Void;

  override public function knock(d: Float): Void;

  override public function land(): Void;

  override public function onWakeUp(): Void;

  override public function update(): Void;
}
