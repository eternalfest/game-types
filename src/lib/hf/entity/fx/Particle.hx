package hf.entity.fx;

import hf.entity.Mover;
import hf.Entity;
import hf.mode.GameMode;

extern class Particle extends Mover {
  public var pid: Int;
  public var bounce: Float;
  public var subFrame: Int;
  public var skipWallsY: Float;

  public function new(): Void;

  public function initParticle(g: GameMode, frame: Int, x: Float, y: Float): Void;

  public static function attach(g: GameMode, frame: Int, x: Float, y: Float): Particle;

  override public function endUpdate(): Void;

  override public function hit(e: Entity): Void;

  override public function init(g: GameMode): Void;

  override public function onDeathLine(): Void;

  override public function onHitCeil(): Void;

  override public function onHitGround(h: Float): Void;

  override public function onHitWall(): Void;

  override public function postfix(): Void;

  override public function prefix(): Void;

  override public function update(): Void;
}
