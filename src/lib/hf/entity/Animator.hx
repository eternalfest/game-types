package hf.entity;

import etwin.flash.DynamicMovieClip;
import hf.entity.Trigger;
import hf.mode.GameMode;

typedef Anim = {
  var id: Int;
  var loop: Bool;
}

extern class Animator extends Trigger {
  public var sub: Null<DynamicMovieClip>;
  public var frame: Float;
  public var animId: Int;
  public var fl_anim: Bool;
  public var animFactor: Float;
  public var fl_loop: Bool;
  public var fl_blinking: Bool;
  public var fl_blinked: Bool;
  public var fl_stickyAnim: Bool;
  public var fl_alphaBlink: Bool;
  public var fl_blink: Bool;
  public var blinkTimer: Float;
  public var blinkColor: Int;
  public var blinkAlpha: Float;
  public var blinkColorAlpha: Float;
  public var fadeStep: Float;

  public function new(): Void;

  public function enableAnimator(): Void;

  public function disableAnimator(): Void;

  public function blink(duration: Float): Void;

  public function stopBlink(): Void;

  public function blinkLife(): Void;

  public function setSub(mc: DynamicMovieClip): Void;

  public function onEndAnim(id: Int): Void;

  public function stickAnim(): Void;

  public function unstickAnim(): Void;

  public function playAnim(animObject: Anim): Void;

  public function forceLoop(flag: Bool): Void;

  public function replayAnim(): Void;

  override public function init(g: GameMode): Void;

  override public function update(): Void;
}
