package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

extern class Blue extends PlayerBomb {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Blue;

  override public function duplicate(): Blue;

  override public function onExplode(): Void;
}
