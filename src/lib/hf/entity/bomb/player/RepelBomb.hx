package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

extern class RepelBomb extends PlayerBomb {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): RepelBomb;

  override public function duplicate(): RepelBomb;

  override public function onExplode(): Void;

  override public function onKick(p: Player): Void;
}
