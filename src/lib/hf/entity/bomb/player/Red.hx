package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

extern class Red extends PlayerBomb {
  public var JUMP_POWER: Float;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Red;

  override public function duplicate(): Red;

  override public function onExplode(): Void;
}
