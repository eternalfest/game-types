package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

extern class PoireBombFrozen extends PlayerBomb {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): PoireBombFrozen;

  override public function duplicate(): PoireBombFrozen;

  override public function onExplode(): Void;

  override public function onHitWall(): Void;
}
