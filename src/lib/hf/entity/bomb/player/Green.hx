package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

extern class Green extends PlayerBomb {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Green;

  override public function duplicate(): Green;

  override public function endUpdate(): Void;

  override public function onExplode(): Void;
}
