package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

extern class Black extends PlayerBomb {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Black;

  override public function duplicate(): Black;

  override public function onExplode(): Void;
}
