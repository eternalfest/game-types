package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

extern class Classic extends PlayerBomb {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Classic;

  override public function duplicate(): Classic;

  override public function onExplode(): Void;
}
