package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.mode.GameMode;

extern class MineFrozen extends PlayerBomb {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): MineFrozen;

  override public function duplicate(): MineFrozen;

  override public function onExplode(): Void;

  override public function onHitWall(): Void;
}
