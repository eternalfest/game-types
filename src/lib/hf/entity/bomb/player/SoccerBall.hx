package hf.entity.bomb.player;

import hf.entity.bomb.PlayerBomb;
import hf.entity.Player;
import hf.mode.GameMode;

extern class SoccerBall extends PlayerBomb {
  public static var TOP_SPEED: Float;
  public var lastPlayer: Null<Player>;
  public var burnTimer: Float;
  public var speed: Float;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): SoccerBall;

  public function burn(): Void;

  override public function duplicate(): SoccerBall;

  override public function endUpdate(): Void;

  override public function infix(): Void;

  override public function init(g: GameMode): Void;

  override public function onExplode(): Void;

  override public function onHitWall(): Void;

  override public function onKick(p: Player): Void;

  override public function update(): Void;

  override public function upgradeBomb(p: Player): Void;
}
