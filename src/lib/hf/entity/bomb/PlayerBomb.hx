package hf.entity.bomb;

import hf.entity.Bomb;
import hf.mode.GameMode;

extern class PlayerBomb extends Bomb {
  public static var UPGRADE_FACTOR: Float;
  public static var MAX_UPGRADES: Int;

  public var fl_unstable: Bool;
  public var upgrades: Int;
  public var owner: Null<Player>;

  public function new(): Void;

  public function setOwner(p: Player): Void;

  public function upgradeBomb(p: Player): Void;

  override public function hit(e: Entity): Void;

  override public function init(g: GameMode): Void;

  override public function onExplode(): Void;

  override public function onHitGround(h: Float): Void;

  override public function onKick(p: Player): Void;

  override public function onLifeTimer(): Void;

  override public function update(): Void;
}
