package hf.entity.bomb.bad;

import hf.entity.bomb.BadBomb;
import hf.entity.Player;
import hf.mode.GameMode;

extern class PoireBomb extends BadBomb {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): PoireBomb;

  override public function duplicate(): PoireBomb;

  override public function onExplode(): Void;

  override public function onKick(p: Player): Void;
}
