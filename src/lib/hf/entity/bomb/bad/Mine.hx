package hf.entity.bomb.bad;

import hf.entity.bomb.BadBomb;
import hf.entity.Player;
import hf.entity.Animator.Anim;
import hf.mode.GameMode;

extern class Mine extends BadBomb {
  public static var SUDDEN_DEATH: Float;
  public static var HIDE_SPEED: Float;
  public static var DETECT_RADIUS: Float;

  public var fl_trigger: Bool;
  public var fl_defuse: Bool;
  public var fl_plant: Bool;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Mine;

  public function triggerMine(): Void;

  override public function duplicate(): Mine;

  override public function initBomb(g: GameMode, x: Float, y: Float): Void;

  override public function onExplode(): Void;

  override public function onHitGround(h: Float): Void;

  override public function onKick(p: Player): Void;

  override public function playAnim(a: Anim): Void;

  override public function update(): Void;
}
