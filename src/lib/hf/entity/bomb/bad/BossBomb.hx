package hf.entity.bomb.bad;

import hf.entity.bomb.BadBomb;
import hf.entity.Player;
import hf.mode.GameMode;

extern class BossBomb extends BadBomb {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): BossBomb;

  override public function duplicate(): BossBomb;

  override public function initBomb(g: GameMode, x: Float, y: Float): Void;

  override public function onExplode(): Void;

  override public function onKick(p: Player): Void;
}
