package hf.entity.bomb;

import hf.entity.Bad;
import hf.entity.Bomb;
import hf.entity.Player;
import hf.mode.GameMode;

extern class BadBomb extends Bomb {
  public var owner: Null<Bad>;

  public function new(): Void;

  public function setOwner(b: Bad): Void;

  public function getFrozen(uid: Int): Null<Bomb>;

  override public function initBomb(g: GameMode, x: Float, y: Float): Void;

  override public function init(g: GameMode): Void;

  override public function onExplode(): Void;

  override public function onKick(p: Player): Void;
}
