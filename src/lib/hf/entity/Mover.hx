package hf.entity;

import hf.entity.Physics;
import hf.mode.GameMode;

typedef NextAction = {
  var dx: Float;
  var dy: Float;
  var delay: Float;
  var action: Int;
}

extern class Mover extends Physics {
  public var fl_bounce: Bool;
  public var bounceFactor: Float;
  public var next: NextAction;

  public function new(): Void;

  public function setNext(dx: Float, dy: Float, delay: Float, action: Int): Void;

  public function onNext(): Void;

  public function isReady(): Bool;

  override public function init(g: GameMode): Void;

  override public function onHitGround(h: Float): Void;

  override public function onKill(): Void;

  override public function update(): Void;
}
