package hf.entity.item;

import hf.entity.Item;
import hf.mode.GameMode;

extern class ScoreItem extends Item {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float, id: Int, subId: Null<Int>): ScoreItem;

  override public function execute(p: Player): Void;

  override public function init(g: GameMode): Void;

  override public function initItem(g: GameMode, x: Float, y: Float, i: Int, si: Null<Int>): Void;
}
