package hf.entity.item;

import hf.entity.Item;
import hf.entity.Player;
import hf.mode.GameMode;

extern class SpecialItem extends Item {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float, id: Int, subId: Null<Int>): SpecialItem;

  override public function execute(p: Player): Void;

  override public function init(g: GameMode): Void;

  override public function update(): Void;
}
