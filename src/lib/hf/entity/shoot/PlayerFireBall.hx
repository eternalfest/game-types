package hf.entity.shoot;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

extern class PlayerFireBall extends Shoot {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): PlayerFireBall;

  override public function destroy(): Void;

  override public function hit(e: Entity): Void;

  override public function init(g: GameMode): Void;

  override public function update(): Void;
}
