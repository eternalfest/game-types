package hf.entity.shoot;

import hf.entity.bad.walker.Framboise;
import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

extern class FramBall extends Shoot {
  public var turnSpeed: Float;
  public var fl_arrived: Bool;
  public var ang: Float;
  public var white: Float;
  public var owner: Null<Framboise>;

  public function new(): Void;

  public function setOwner(b: Framboise): Void;

  public static function attach(g: GameMode, x: Float, y: Float): FramBall;

  public function adjustAngRad(a: Float): Float;

  public function onArrived(): Void;

  override public function endUpdate(): Void;

  override public function hit(e: Entity): Void;

  override public function infix(): Void;

  override public function onLifeTimer(): Void;

  override public function update(): Void;
}
