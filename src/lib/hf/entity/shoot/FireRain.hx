package hf.entity.shoot;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

extern class FireRain extends Shoot {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): FireRain;

  public function hitLevel(): Void;

  override public function destroy(): Void;

  override public function hit(e: Entity): Void;

  override public function init(g: GameMode): Void;

  override public function onDeathLine(): Void;

  override public function onHitGround(h: Float): Void;

  override public function onHitWall(): Void;

  override public function update(): Void;
}
