package hf.entity.shoot;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

extern class Pepin extends Shoot {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Pepin;

  override public function hit(e: Entity): Void;
}
