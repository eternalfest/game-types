package hf.entity.shoot;

import hf.entity.Player;
import hf.entity.Shoot;
import hf.mode.GameMode;

extern class Hammer extends Shoot {
  public var player: Null<Player>;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Hammer;

  public function setOwner(p: Player): Void;

  override public function destroy(): Void;

  override public function endUpdate(): Void;

  override public function init(g: GameMode): Void;

  override public function update(): Void;
}
