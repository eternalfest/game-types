package hf.entity.shoot;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

extern class FireBall extends Shoot {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): FireBall;

  override public function destroy(): Void;

  override public function hit(e: Entity): Void;

  override public function init(g: GameMode): Void;
}
