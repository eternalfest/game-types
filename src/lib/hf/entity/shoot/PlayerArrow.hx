package hf.entity.shoot;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

extern class PlayerArrow extends Shoot {
  public var fl_livedOneTurn: Bool;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): PlayerArrow;

  override public function hit(e: Entity): Void;

  override public function init(g: GameMode): Void;

  override public function onHitWall(): Void;

  override public function update(): Void;
}
