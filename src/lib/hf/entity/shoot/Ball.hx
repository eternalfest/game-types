package hf.entity.shoot;

import hf.entity.bad.walker.Fraise;
import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

extern class Ball extends Shoot {
  public var targetCatcher: Null<Fraise>;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Ball;

  override public function hit(e: Entity): Void;

  override public function init(g: GameMode): Void;

  override public function onLifeTimer(): Void;
}
