package hf.entity.shoot;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

extern class PlayerPearl extends Shoot {
  public var shotList: Array<Int>;
  public var fl_bounceBorders: Bool;

  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): PlayerPearl;

  public function hasBeenShot(id: Int): Bool;

  public function hitWallAnim(): Void;

  override public function destroy(): Void;

  override public function endUpdate(): Void;

  override public function hit(e: Entity): Void;

  override public function init(g: GameMode): Void;

  override public function onLifeTimer(): Void;

  override public function onSideBorderBounce(): Void;
}
