package hf.entity.shoot;

import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

extern class Zeste extends Shoot {
  public function new(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Zeste;

  override public function hit(e: Entity): Void;

  override public function moveDown(s: Float): Void;
}
