package hf.entity.shoot;

import hf.entity.boss.Bat;
import hf.entity.Shoot;
import hf.Entity;
import hf.mode.GameMode;

extern class BossFireBall extends Shoot {
  public var turnSpeed: Float;
  public var distSpeed: Float;
  public var dist: Float;
  public var maxDist: Float;
  public var bat: Bat;
  public var ang: Float;

  public function new(): Void;

  public function initBossShoot(b: Bat, a: Float): Void;

  public function center(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): BossFireBall;

  override public function destroy(): Void;

  override public function hit(e: Entity): Void;

  override public function init(g: GameMode): Void;

  override public function update(): Void;
}
