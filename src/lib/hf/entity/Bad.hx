package hf.entity;

import etwin.flash.MovieClip;
import hf.entity.Mover;
import hf.entity.Player;
import hf.entity.Animator.Anim;
import hf.Entity;
import hf.mode.GameMode;

extern class Bad extends Mover {
  public var player: Null<Player>;
  public var fl_playerClose: Null<Bool>;
  public var comboId: Null<Int>;
  public var freezeTimer: Float;
  public var freezeTotal: Null<Float>;
  public var fl_freeze: Bool;
  public var knockTimer: Float;
  public var fl_knock: Bool;
  public var deathTimer: Null<Float>;
  public var yTrigger: Null<Float>;
  public var fl_ninFoe: Bool;
  public var fl_ninFriend: Bool;
  public var fl_showIA: Bool;
  public var fl_trap: Bool;
  public var closeDistance: Float;
  public var realRadius: Float;
  public var speedFactor: Float;
  public var anger: Int;
  public var angerFactor: Float;
  public var maxAnger: Int;
  public var chaseFactor: Float;
  public var iceMc: MovieClip;

  public function new(): Void;

  public function initBad(g: GameMode, x: Float, y: Float): Void;

  public function forceKill(dx: Null<Float>): Void;

  public function burn(): Void;

  public function isHealthy(): Bool;

  public function setCombo(id: Int): Void;

  public function calcSpeed(): Void;

  public function updateSpeed(): Void;

  public dynamic function dropReward(): Void;

  public function evaluateSpeed(): Float;

  public function onMelt(): Void;

  public function onWakeUp(): Void;

  public function onFreeze(): Void;

  public function onKnock(): Void;

  public function onHurryUp(): Void;

  public function freeze(timer: Float): Void;

  public function knock(timer: Float): Void;

  public function melt(): Void;

  public function wakeUp(): Void;

  public function calmDown(): Void;

  public function angerMore(): Void;

  public function hate(p: Player): Void;

  override public function destroy(): Void;

  override public function endUpdate(): Void;

  override public function hit(e: Entity): Void;

  override public function init(g: GameMode): Void;

  override public function killHit(dx: Null<Float>): Void;

  override public function onDeathLine(): Void;

  override public function onHitGround(h: Float): Void;

  override public function onKill(): Void;

  override public function playAnim(o: Anim): Void;

  override public function needsPatch(): Bool;

  override public function update(): Void;
}
