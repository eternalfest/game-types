package hf.entity;

import hf.entity.Mover;
import hf.entity.Player;
import hf.Entity;
import hf.mode.GameMode;

extern class Bomb extends Mover {
  public var explodeSound: String;
  public var radius: Float;
  public var power: Float;
  public var duration: Float;
  public var fl_explode: Bool;
  public var fl_airKick: Bool;
  public var fl_bumped: Bool;

  public function new(): Void;

  public function initBomb(g: GameMode, x: Float, y: Float): Void;

  public function bombGetClose(type: Int): Array<Entity>;

  public function onExplode(): Void;

  public function onKick(p: Player): Void;

  public function duplicate(): Bomb;

  override public function endUpdate(): Void;

  override public function onBump(): Void;

  override public function onDeathLine(): Void;

  override public function onEndAnim(id: Int): Void;

  override public function onHitWall(): Void;

  override public function onLifeTimer(): Void;

  override public function onPortal(pid: Int): Void;

  override public function onPortalRefusal(): Void;

  override public function needsPatch(): Bool;
}
