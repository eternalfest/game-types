package hf.entity;

import hf.Animation;
import hf.Entity;
import hf.Pos;
import hf.entity.Bomb;
import hf.entity.bomb.PlayerBomb;
import hf.entity.Physics;
import hf.entity.PlayerController;
import hf.entity.Animator.Anim;
import hf.mode.GameMode;
import hf.SpecialManager;

typedef KickedBomb = {
  var t: Float;
  var bid: Int;
}

extern class Player extends Physics {
  public var name: String;
  public var baseWalkAnim: Anim;
  public var baseStopAnim: Anim;
  public var score: Int;
  public var scoreCS: Int;
  public var speedFactor: Float;
  public var extraLifeCurrent: Int;
  public var fl_chourou: Bool;
  public var fl_carot: Bool;
  public var fl_candle: Bool;
  public var defaultHead: Int;
  public var head: Int;
  public var fl_knock: Bool;
  public var knockTimer: Float;
  public var skin: Int;
  public var oxygen: Float;
  public var recentKicks: Array<KickedBomb>;
  public var currentWeapon: Int;
  public var lastBomb: Int;
  public var maxBombs: Int;
  public var initialMaxBombs: Int;
  public var coolDown: Float;
  public var lives: Int;
  public var fl_torch: Bool;
  public var baseColor: Int;
  public var darkColor: Int;
  public var fl_lockControls: Bool;
  public var lockTimer: Float;
  public var fl_entering: Bool;
  public var extendList: Array<Bool>;
  public var extendOrder: Array<Int>;
  public var pid: Null<Int>; // Set by `insertPlayer`
  public var edgeTimer: Float;
  public var waitTimer: Float;
  public var dbg_grid: Int;
  public var debugInput: String;
  public var ctrl: Null<PlayerController>;
  public var specialMan: Null<SpecialManager>;
  public var dbg_lastKey: Int;
  public var fl_shield: Bool;
  public var shieldMC: Animation;
  public var shieldTimer: Float;
  public var startX: Float;
  public var bounceLimit: Int;

  public function new(): Void;

  public function initPlayer(g: GameMode, x: Float, y: Float): Void;

  public function getDebugControls(): Void;

  public static function attach(g: GameMode, x: Float, y: Float): Player;

  public function forceKill(dx: Null<Float>): Void;

  public dynamic function getScore(origin: Pos<Float>, value: Int): Void;

  public dynamic function getScoreHidden(value: Int): Void;

  public function getExtend(id: Int): Void;

  public function killPlayer(): Void;

  public function shield(duration: Null<Float>): Void;

  public function unshield(): Void;

  public function knock(d: Float): Void;

  public function curse(id: Int): Void;

  public function setBaseAnims(a_walk: Anim, a_stop: Anim): Void;

  public function showTeleporters(): Void;

  public function lockControls(d: Float): Void;

  public function isRecentKick(b: Bomb): Bool;

  public function attack(): Null<Entity>;

  public function isBombWeapon(id: Int): Bool;

  public function isShootWeapon(id: Int): Bool;

  public function drop(b: PlayerBomb): PlayerBomb;

  public function airJump(): Void;

  public function shoot(s: Shoot): Shoot;

  public function changeWeapon(id: Int): Void;

  public function kickBomb(l: Array<Bomb>, powerFactor: Float): Void;

  public function upKickBomb(l: Array<Bomb>): Void;

  public function countBombs(): Int;

  public function onShieldOut(): Void;

  public function onWakeUp(): Void;

  public function onNextLevel(): Void;

  public function onStartLevel(): Void;

  override public function destroy(): Void;

  override public function endUpdate(): Void;

  override public function hide(): Void;

  override public function hit(e: Entity): Void;

  override public function infix(): Void;

  override public function init(g: GameMode): Void;

  override public function killHit(dx: Null<Float>): Void;

  override public function onBump(): Void;

  override public function onDeathLine(): Void;

  override public function onEndAnim(id: Int): Void;

  override public function onHitGround(h: Float): Void;

  override public function onHitWall(): Void;

  override public function onPortal(pid: Int): Void;

  override public function onPortalRefusal(): Void;

  override public function onTeleport(): Void;

  override public function playAnim(a: Anim): Void;

  override public function resurrect(): Void;

  override public function scale(n: Float): Void;

  override public function show(): Void;

  override public function needsPatch(): Bool;

  override public function update(): Void;
}
