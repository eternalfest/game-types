package hf.entity;

import hf.entity.Player;
import hf.mode.GameMode;

extern class PlayerController {
  public var lastKeys: Array<Int>;
  public var keyLocks: Array<Bool>;
  public var alts: Array<Int>;
  public var player: Player;
  public var game: GameMode;
  public var fl_upKick: Bool;
  public var fl_powerControl: Bool;
  public var walkTimer: Float;
  public var waterJump: Int;
  public var attack: Int;
  public var jump: Int;
  public var down: Int;
  public var left: Int;
  public var right: Int;

  public function new(p: Player): Void;

  public function setKeys(j: Int, d: Int, l: Int, r: Int, a: Int): Void;

  public function setAlt(id: Int, idAlt: Int): Void;

  public function keyIsDown(id: Int): Bool;

  public function lockKey(id: Int): Void;

  public function getControls(): Void;

  public function update(): Void;
}
