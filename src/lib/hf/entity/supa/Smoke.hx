package hf.entity.supa;

import hf.entity.Supa;
import hf.mode.GameMode;

extern class Smoke extends Supa {
  public function new(): Void;

  public static function attach(g: GameMode): Smoke;

  override public function initSupa(g: GameMode, x: Float, y: Float): Void;
}
