package hf.entity.supa;

import hf.entity.Supa;
import hf.mode.GameMode;

extern class Bubble extends Supa {
  public function new(): Void;

  public static function attach(g: GameMode): Bubble;

  override public function initSupa(g: GameMode, x: Float, y: Float): Void;

  override public function postfix(): Void;

  override public function prefix(): Void;
}
