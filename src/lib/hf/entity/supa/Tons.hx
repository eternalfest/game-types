package hf.entity.supa;

import hf.entity.Supa;
import hf.mode.GameMode;

extern class Tons extends Supa {
  public function new(): Void;

  public static function attach(g: GameMode): Tons;

  override public function initSupa(g: GameMode, x: Float, y: Float): Void;

  override public function postfix(): Void;

  override public function prefix(): Void;
}
