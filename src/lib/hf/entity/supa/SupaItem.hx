package hf.entity.supa;

import hf.entity.Player;
import hf.entity.Supa;
import hf.mode.GameMode;

extern class SupaItem extends Supa {
  public var supaId: Int;

  public function new(): Void;

  public static function attach(g: GameMode, id: Int): SupaItem;

  public function pick(pl: Player): Void;

  override public function initSupa(g: GameMode, x: Float, y: Float): Void;

  override public function onDeathLine(): Void;

  override public function prefix(): Void;
}
