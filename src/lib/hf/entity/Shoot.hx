package hf.entity;

import hf.entity.Physics;
import hf.mode.GameMode;

extern class Shoot extends Physics {
  public var fl_checkBounds: Bool;
  public var fl_borderBounce: Bool;
  public var coolDown: Float;
  // TODO: Remove `shootSpeed` from this class and define it only on classes using it
  public var shootSpeed: Float;

  public function new(): Void;

  public function initShoot(g: GameMode, x: Float, y: Float): Void;

  public function onSideBorderBounce(): Void;

  public function onHorizontalBorderBounce(): Void;

  override public function endUpdate(): Void;

  override public function init(g: GameMode): Void;

  override public function update(): Void;
}
