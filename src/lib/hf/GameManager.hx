package hf;

import etwin.flash.DynamicMovieClip;
import etwin.flash.Sound;
import hf.Hf;
import hf.Cookie;
import hf.DepthManager;
import hf.GameParameters;
import hf.Hash;
import hf.mode.GameMode;
import hf.Mode;
import hf.SoundManager;

extern class GameManager {
  public static var BASE_VOLUME: Float;
  public static var CONFIG: GameParameters;
  public static var KEY: Int;
  public static var HH: Hash;
  public static var SELF: GameManager;

  public var root: Hf;
  public var uniq: Int;
  public var fl_local: Bool;
  public var fl_cookie: Bool;
  public var musics: Array<Sound>;
  public var history: Array<String>;
  public var depthMan: DepthManager;
  public var soundMan: SoundManager;
  public var fl_debug: Bool;
  public var cookie: Cookie;
  public var fVersion: Int;
  public var fl_flash8: Bool;
  public var progressBar: Null<DynamicMovieClip /* TODO */>;
  public var current: Mode;
  public var child: Null<Mode>;
  public var fps: Float;

  public function new(mc: Hf, initObj: Dynamic /*TODO*/): Void;

  public function registerClasses(): Void;

  public function setExists(n: String): Bool;

  public function progress(ratio: Float): Void;

  public static function fatal(msg: String): Void;

  public static function warning(msg: String): Void;

  public function redirect(url: String, params: Dynamic /*TODO*/): Void;

  public function logIllegal(str: String): Void;

  public function logAction(str: String): Void;

  public function transition(prev: Mode, next: Mode): Void;

  // TODO: Generic
  public function startChild(c: Mode): Mode;

  public function stopChild(data: Dynamic /*TODO*/): Void;

  public function startMode(m: Mode): Void;

  public function startGameMode(m: GameMode): Void;

  public function isAdventure(): Bool;

  public function isTutorial(): Bool;

  public function isSoccer(): Bool;

  public function isMultiCoop(): Bool;

  public function isTimeAttack(): Bool;

  public function isMultiTime(): Bool;

  public function isBossRush(): Bool;

  public function isFjv(): Bool;

  public function isDev(): Bool;

  public function isMode(modeName: String): Bool;

  public function startDefaultGame(): Void;

  public function main(): Void;
}
