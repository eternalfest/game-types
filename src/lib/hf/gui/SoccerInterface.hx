package hf.gui;

import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.mode.GameMode;

extern class SoccerInterface {
  public static var GLOW_COLOR: Int;

  public var mc: DynamicMovieClip /* TODO */;
  public var game: GameMode;
  public var scores: Array<DynamicMovieClip /* TODO */>;
  public var time: DynamicMovieClip /* TODO */;

  public function new(game: GameMode): Void;

  public function init(): Void;

  public function setScore(pid: Int, n: Int): Void;

  public function setTime(str: String): Void;

  public function destroy(): Void;

  public function update(): Void;
}
