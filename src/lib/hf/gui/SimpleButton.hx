package hf.gui;

import hf.gui.Container;
import hf.gui.Item;

extern class SimpleButton extends Item {
  public var event: Dynamic /*TODO*/;
  public var key: Dynamic /*TODO*/;
  public var body: Dynamic /*TODO*/;
  public var left: Dynamic /*TODO*/;
  public var right: Dynamic /*TODO*/;
  public var toggle: Null<Dynamic /*TODO*/>;
  public var fl_lock: Null<Bool>;
  public var fl_keyLock: Null<Bool>;

  public function new(): Void;

  public function initButton(c: Container, l: Dynamic /*TODO*/, key: Dynamic /*TODO*/, func: Dynamic /*TODO*/): Void;

  public function setToggleKey(k: Dynamic /*TODO*/): Void;

  public function release(): Void;

  public function rollOut(): Void;

  public function rollOver(): Void;

  public function shortcut(): Bool;

  public static function attach(c: Container, l: Dynamic /*TODO*/, k: Dynamic /*TODO*/, func: Dynamic /*TODO*/): SimpleButton;

  override public function setLabel(l: String): Void;

  override public function update(): Void;
}
