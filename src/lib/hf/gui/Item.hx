package hf.gui;

import etwin.flash.MovieClip;
import hf.gui.Container;

extern class Item extends MovieClip {
  public var field: Dynamic /*TODO*/;
  public var width: Dynamic /*TODO*/;
  public var container: Container;

  public function new(): Void;

  public function setLabel(l: String): Void;

  public function scale(ratio: Float): Void;

  public function init(c: Container, l: String): Void;

  public function update(): Void;
}
