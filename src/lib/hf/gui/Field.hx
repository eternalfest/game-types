package hf.gui;

import hf.gui.Item;

extern class Field extends Item {
  public var bg: Dynamic /*TODO*/;

  public function new(): Void;

  public function initField(c: Container): Void;

  public function setWidth(w: Float): Void;

  public function setField(s: String): Void;

  public function getField(): String;

  public static function attach(c: Container): Field;

  override public function setLabel(s: String): Void;
}
