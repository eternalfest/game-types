package hf.gui;

import etwin.flash.MovieClip;
import hf.DepthManager;

extern class Container {
  public var mode: Dynamic /*TODO*/;
  public var mc: MovieClip;
  public var depthMan: DepthManager;
  public var width: Float;
  public var currentX: Float;
  public var currentY: Float;
  public var scale: Float;
  public var lineHeight: Float;
  public var list: Array<Dynamic /*TODO*/>;
  public var fl_lock: Bool;

  public static var MARGIN: Float;
  public static var MIN_HEIGHT: Float;

  public function new(m: Dynamic /*TODO*/, x: Float, y: Float, wid: Float): Void;

  public function lock(): Void;

  public function unlock(): Void;

  public function insert(b: Dynamic /*TODO*/): Dynamic /*TODO*/;

  public function endLine(): Void;

  public function setScale(ratio: Float): Void;

  public function update(): Void;
}
