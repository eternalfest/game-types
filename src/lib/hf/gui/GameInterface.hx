package hf.gui;

import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.mode.GameMode;

extern class GameInterface {
  public static var GLOW_COLOR: Int;
  public static var BASE_X: Float;
  public static var BASE_X_RIGHT: Float;
  public static var BASE_WIDTH: Float;
  public static var MAX_LIVES: Int;

  public var mc: DynamicMovieClip /* TODO */;
  public var game: GameMode;
  public var more: Array<MovieClip>;
  public var fl_light: Bool;
  public var fl_print: Bool;
  public var level: DynamicMovieClip /* TODO */;
  public var scores: Array<DynamicMovieClip /* TODO */>;
  public var baseColor: Int;
  public var fl_multi: Null<Bool>;
  public var letters: Array<Array<MovieClip>>;
  public var fakeScores: Array<Int>;
  public var realScores: Array<Int>;
  public var currentLives: Array<Int>;
  public var lives: Array<Array<MovieClip>>;

  public function new(game: GameMode): Void;

  public function initSingle(): Void;

  public function initMulti(): Void;

  public function initTime(): Void;

  public function lightMode(): Void;

  public function setScore(pid: Int, v: Int): Void;

  public function getScoreTxt(v: Int): String;

  public function setLevel(id: Int): Void;

  public function hideLevel(): Void;

  public function setLives(pid: Int, v: Int): Void;

  public function print(pid: Int, s: String): Void;

  public function cls(): Void;

  public function getExtend(pid: Int, id: Int): Void;

  public function clearExtends(pid: Int): Void;

  public function destroy(): Void;

  public function update(): Void;
}
