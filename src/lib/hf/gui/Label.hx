package hf.gui;

import hf.gui.Item;

extern class Label extends Item {
  public function new(): Void;

  public static function attach(c: Container, l: String): Label;
}
