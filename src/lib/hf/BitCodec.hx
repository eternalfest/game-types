package hf;

extern class BitCodec {
  public var error_flag: Bool;
  public var data: String;
  public var in_pos: Int;
  public var nbits: Int;
  public var bits: Int;
  public var crc: Int;

  public function new(): Void;

  public function crcStr(): String;

  public function setData(d: String): Void;

  public function read(n: Int): Int;

  public function nextPart(): Void;

  public function hasError(): Bool;

  public function toString(): String;

  public function write(n: Int, b: Int): Void;

  public static function ord(code: String): Int;

  public static function chr(code: Int): String;

  public static function eif(code: String): Null<Int>;

  public static function dif(code: Int): String;
}
