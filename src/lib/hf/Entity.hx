package hf;

import etwin.flash.Color;
import etwin.flash.MovieClip;
import hf.levels.GameMechanics;
import hf.mode.GameMode;
import hf.Pos;

extern class Entity extends MovieClip {
  public var game: GameMode;

  /**
   * Unique id.
   *
   * This integer uniquely identifies the entity instance within the current
   * game mode.
   */
  public var uniqId: Int;
  public var parent: Null<Entity>;
  public var world: Null<GameMechanics>;

  /**
   * Bitset of types.
   */
  public var types: Int;

  public var x: Float;
  public var y: Float;
  public var oldX: Float;
  public var oldY: Float;
  public var cx: Int;
  public var cy: Int;
  public var fcx: Int;
  public var fcy: Int;
  public var dir: Null<Int>;
  public var dx: Null<Float>;
  public var dy: Null<Float>;
  public var alpha: Float;
  public var rotation: Float;
  public var minAlpha: Float;
  public var blendId: Int;
  public var _xOffset: Float;
  public var _yOffset: Float;
  public var scaleFactor: Float;
  public var color: Color;
  public var totalLife: Float;
  public var lifeTimer: Float;
  public var fl_kill: Bool;
  public var fl_destroy: Bool;
  public var fl_stick: Bool;
  public var sticker: MovieClip;
  public var stickerX: Float;
  public var stickerY: Float;
  public var stickTimer: Float;
  public var fl_elastick: Bool;
  public var elaStickFactor: Float;
  public var fl_stickRot: Bool;
  public var fl_stickBound: Bool;
  public var fl_softRecal: Bool;
  public var softRecalFactor: Float;
  public var defaultBlend: Dynamic /*TODO*/;
  public var scriptId: Null<Int>;

  public function new(): Void;

  public function init(g: GameMode): Void;

  public function register(type: Int): Void;

  public function unregister(type: Int): Void;

  public function isType(t: Int): Bool;

  public function setParent(e: Entity): Void;

  public function setLifeTimer(t: Float): Void;

  public function updateLifeTimer(t: Float): Void;

  public function onLifeTimer(): Void;

  public function hitBound(e: Entity): Bool;

  public function hit(e: Entity): Void;

  public function destroy(): Void;

  public function stick(mc: MovieClip, ox: Float, oy: Float): Void;

  public function setElaStick(f: Float): Void;

  public function unstick(): Void;

  public function activateSoftRecal(): Void;

  public function release(): Void;

  public function rollOver(): Void;

  public function rollOut(): Void;

  public function hide(): Void;

  public function show(): Void;

  public function scale(n: Float): Void;

  public function setColorHex(a: Float, col: Int): Void;

  public function resetColor(): Void;

  public function setBlend(m: Int): Void;

  public function updateCoords(): Void;

  public static function rtc(x: Float, y: Float): Pos<Int>;

  public static function x_rtc(n: Float): Int;

  public static function y_rtc(n: Float): Int;

  public static function x_ctr(n: Float): Float;

  public static function y_ctr(n: Float): Float;

  public function adjustAngle(a: Float): Float;

  public function adjustToLeft(): Void;

  public function adjustToRight(): Void;

  public function centerInCase(): Void;

  public function distanceCase(cx: Float, cy: Float): Float;

  public function distance(x: Float, y: Float): Float;

  public function update(): Void;

  public function short(): String;

  public function printTypes(): String;

  public function endUpdate(): Void;
}
