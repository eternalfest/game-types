package hf;

extern class RandomManager {
  public var bulks: Array<Array<Int>>;
  public var expanded: Array<Array<Int>>;
  public var sums: Array<Int>;

  public function new(): Void;

  public function register(id: Int, bulk: Array<Int>): Void;

  public function computeSum(id: Int): Void;

  public function expand(id: Int): Void;

  public function draw(id: Int): Null<Int>;

  public function drawSpecial(): Void;

  public function evaluateChances(id: Int, value: Int): Float;

  public function remove(rid: Int, id: Int): Void;
}
