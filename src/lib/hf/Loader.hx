package hf;

import etwin.flash.LoadVars;
import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import etwin.flash.MovieClipLoader;
import etwin.flash.Sound;
import etwin.flash.XMLNode;

extern class Loader {
  public var root_mc: DynamicMovieClip /* TODO */;
  public var timeOutId: Int;
  public var musicTimeOut: Float;
  public var nearEndTimer: Float;
  public var fVersion: Int;
  public var fl_flash8: Bool;
  public var rawLang: String;
  public var xmlLang: XMLNode;
  public var _options: Dynamic /*TODO*/;
  public var _mode: Dynamic /*TODO*/;
  public var testPlayer6: Dynamic /*TODO*/;
  public var lv: LoadVars;
  public var _swf: String;
  public var _srv: String;
  public var _musics: Array<String>;
  public var musicId: Int;
  public var musics: Array<Sound>;
  public var game: MovieClip;
  public var music_mc: MovieClip;
  public var loading: Dynamic /*TODO*/;
  public var mcl: MovieClipLoader;
  public var fl_gameOver: Bool;
  public var fl_fade: Bool;
  public var fl_exit: Bool;
  public var save_score: LoadVars;
  public var uniqueItems: Int;
  public var fullurl: String;
  public var sendData: Void -> Void;
  public var retry_id: Float;
  public var exitUrl: String;
  public var exitParams: Dynamic /*TODO*/;
  public var gameInst: Dynamic /*TODO*/;
  public var _qid: Null<String>;
  public var _mid: Null<String>;
  public var _gid: Null<String>;
  public var _key: Null<String>;
  public var _families: Dynamic /*TODO*/;
  public var fl_saveAgain: Bool;

  public static var BASE_SCRIPT_URL: String;
  public static var BASE_SCRIPT_URL_ALT: String;
  public static var BASE_SWF_URL: String;
  public static var BASE_MUSIC_URL: String;
  public static var TIMEOUT: Float;
  public static var MUSIC_TIMEOUT: Float;
  public static var MUSIC_NEAR_END: Float;
  public static var URL_KEY: String;

  public function new(mc: DynamicMovieClip /* TODO */): Void;

  public function initLoader(): Void;

  public function loadMusic(): Void;

  public function gameLoadDone(): Void;

  public function musicLoadDone(fl: Bool): Void;

  public function gameReady(): Void;

  public function menu(url: Null<String>): Void;

  public function makeUrl(url: String, params: Dynamic /*TODO*/): String;

  public function getRunId(): Dynamic /*TODO*/;

  public function queryStart(): Void;

  public function serverData(s: Null<String>): Void;

  public function startGame(): Void;

  public function countUniques(a: Array<Dynamic /*TODO*/>): Int;

  public function asc(c: String): Int;

  public function gameOver(score: Dynamic /*TODO*/, runId: Dynamic /*TODO*/, stats: Dynamic /*TODO*/): Void;

  public function saveAgain(): Void;

  public function exitGame(url: String, params: Dynamic /*TODO*/): Void;

  public function replace(str: Null<String>, search: String, replace: String): Null<String>;

  public function checkBaseUrl___(): Bool;

  public function decodeUrl(url: String): String;

  public function initBaseUrl(): Bool;

  public function error(msg: String): Void;

  public function initMachineId___(): Void;

  public function isLocal(): Bool;

  public function isDev(): Bool;

  public function isMode(modeName: String): Bool;

  public function getLangStr(id: Int): String;

  public function getStupidTrackName(): String;

  public function attachLoading(frame: Int): Void;

  public function redirect(url: String): Void;

  public dynamic function main(): Void;

  public dynamic function mainGame(): Void;
}
