package hf;

extern class BlendMode {
  public static var NORMAL: Int;
  public static var LAYER: Int;
  public static var MULTIPLY: Int;
  public static var SCREEN: Int;
  public static var LIGHTEN: Int;
  public static var DARKEN: Int;
  public static var DIFFERENCE: Int;
  public static var ADD: Int;
  public static var SUBSTRACT: Int;
  public static var INVERT: Int;
  public static var ALPHA: Int;
  public static var ERASE: Int;
  public static var OVERLAY: Int;
  public static var HARDLIGHT: Int;
}
