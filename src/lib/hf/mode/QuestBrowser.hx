package hf.mode;

import etwin.flash.TextField;
import etwin.flash.XMLNode;
import hf.Mode;

extern class QuestBrowser extends Mode {
  public var mcList: Array<Dynamic>;
  public var footer: Dynamic;
  public var qfilter: Int;
  public var fullRandList: Array<Dynamic>;
  public var quests: XMLNode;
  public var desc: TextField;
  public var klock: Dynamic /*TODO*/;

  public static var ITEM_WIDTH: Float;
  public static var ITEM_HEIGHT: Float;

  public function new(m: Dynamic /*TODO*/): Void;

  public function getRandColor(proba: Int): Int;

  public function refresh(): Void;

  public function getQuest(id: Int): Null<XMLNode>;

  override public function endMode(): Void;

  override public function destroy(): Void;

  override public function main(): Void;
}
