package hf.mode;

import hf.gui.Container;
import hf.Mode;

extern class ScriptEditor extends Mode {
  public var fl_focus: Bool;
  public var caretPos: Int;
  public var original: String;
  public var field: Dynamic /*TODO*/;
  public var menuC: Container;
  public var styleC: Container;
  public var fl_lockNext: Bool;
  public var attributes: Dynamic /*TODO*/;
  public var field_path: Dynamic /*TODO*/;
  public var fl_lockPrev: Bool;

  public static var INDENT_STR: String;

  public function new(m: Dynamic /*TODO*/, script: String): Void;

  public function focus(pos: Int): Void;

  public function cleanXml(s: String): String;

  public function isValid(raw: String): Bool;

  public function getAttributes(node: Dynamic /*TODO*/): Dynamic /*TODO*/;

  public function getIndent(level: Int): String;

  public function reIndent(node: Dynamic /*TODO*/, level: Int): String;

  public function append(s: String): Void;

  public function onClear(): Void;

  public function onReIndent(): Void;

  public function onReload(): Void;

  public function onTriggerTimer(): Void;

  public function onTriggerPos(): Void;

  public function onTriggerDo(): Void;

  public function onTriggerEnd(): Void;

  public function onTriggerBirth(): Void;

  public function onTriggerExp(): Void;

  public function onTriggerAttach(): Void;

  public function onTriggerEnter(): Void;

  public function onTriggerNightmare(): Void;

  public function onTriggerMirror(): Void;

  public function onTriggerMulti(): Void;

  public function onTriggerNinja(): Void;

  public function onEventBad(): Void;

  public function onEventScoreItem(): Void;

  public function onEventSpecItem(): Void;

  public function onEventMsg(): Void;

  public function onEventTuto(): Void;

  public function onEventKillMsg(): Void;

  public function onEventPointer(): Void;

  public function onEventKillPointer(): Void;

  public function onEventDecoration(): Void;

  public function onEventMusic(): Void;

  public function onEventKill(): Void;

  public function onEventAddTile(): Void;

  public function onEventRemoveTile(): Void;

  public function onEventGoto(): Void;

  public function onEventHide(): Void;

  public function onEventCodeTrigger(): Void;

  public function onEventPortal(): Void;

  public function onEventItemLine(): Void;

  public function onEventSetVar(): Void;

  public function onEventOpenPortal(): Void;

  public function onEventDarkness(): Void;

  public function onEventFakeLID(): Void;

  override public function init(): Void;

  override public function getControls(): Void;

  override public function endMode(): Void;

  override public function main(): Void;
}
