package hf.mode;

import hf.levels.SetManager;
import hf.Mode;

extern class Converter extends Mode {
  public var currentSet: Int;
  public var world: Null<SetManager>;

  public static var SETS: Array<String>;

  public function new(m: Dynamic /*TODO*/): Void;

  public function readNext(): Void;

  public function convert(): Void;

  public function serialize(d: Dynamic /*TODO*/): String;

  public function unserialize(s: String): Dynamic /*TODO*/;

  override public function main(): Void;
}
