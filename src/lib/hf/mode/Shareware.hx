package hf.mode;

import hf.GameManager;
import hf.mode.Adventure;

extern class Shareware extends Adventure {
  public function new(m: GameManager, id: Int): Void;

  public function darknessManager(): Void;

  override public function endMode(): Void;

  override public function initGame(): Void;

  override public function initWorld(): Void;

  override public function onEndOfSet(): Void;

  override public function onGameOver(): Void;

  override public function onLevelReady(): Void;

  override public function onPause(): Void;
}
