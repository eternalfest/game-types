package hf.mode;

import etwin.flash.MovieClip;
import hf.gui.Container;
import hf.gui.SimpleButton;
import hf.levels.SetManager;
import hf.levels.View;
import hf.Mode;

extern class Editor extends Mode {
  public var fl_menu: Bool;
  public var fl_click: Bool;
  public var fl_modified: Array<Bool>;
  public var badList: Array<Dynamic /*TODO*/>;
  public var badId: Int;
  public var slotList: Array<Dynamic /*TODO*/>;
  public var triggerList: Array<Dynamic /*TODO*/>;
  public var fieldList: Array<Dynamic /*TODO*/>;
  public var fieldId: Int;
  public var tool: Int;
  public var resetConfirm: Float;
  public var firstLevel: Int;
  public var firstSet: Dynamic /*TODO*/;
  public var input: String;
  public var fields: MovieClip;
  public var header: Dynamic /*TODO*/;
  public var footer: Dynamic /*TODO*/;
  public var cursor: Null<Dynamic /*TODO*/>;
  public var borders: Null<Dynamic /*TODO*/>;
  public var lastPoint: Null<Dynamic /*TODO*/>;
  public var view: Null<View>;
  public var world: Null<SetManager>;
  public var menuC: Null<Container>;
  public var logMC: Null<Dynamic /*TODO*/>;
  public var log: Null<Dynamic /*TODO*/>;
  public var tileButton: Null<SimpleButton>;
  public var startButton: Null<SimpleButton>;
  public var badButton: Null<SimpleButton>;
  public var fieldButton: Null<SimpleButton>;
  public var specialSlotButton: Null<SimpleButton>;
  public var scoreSlotButton: Null<SimpleButton>;
  public var fl_save: Null<Bool>;
  public var startMc: Null<Dynamic /*TODO*/>;
  public var bList: Null<Dynamic /*TODO*/>;
  public var currentBad: Null<Dynamic /*TODO*/>;
  public var lastKey: Null<Dynamic /*TODO*/>;
  public var fl_lockFollow: Null<Bool>;
  public var save_currentId: Dynamic /*TODO*/;
  public var save_raw: Dynamic /*TODO*/;
  public var styleBuffer: Dynamic /*TODO*/;
  public var fl_lockToggle: Dynamic /*TODO*/;
  public var cx: Int;
  public var cy: Int;
  public var buffer: Dynamic /*TODO*/;
  public var setName: Dynamic /*TODO*/;
  public var dimensionId: Dynamic /*TODO*/;

  public static var DIMENSIONS: Array<Dynamic /*TODO*/>;

  public function new(m: Dynamic /*TODO*/, fset: Dynamic /*TODO*/, id: Int): Void;

  override public function init(): Void;

  public function loadSet(n: String): Void;

  public function attachMenu(): Void;

  public function toggleMenu(): Void;

  public function display(txt: String): Void;

  public function cls(): Void;

  public function redrawAll(): Void;

  public function redrawView(): Void;

  public function redrawBads(): Void;

  public function redrawSlots(): Void;

  public function redrawScript(): Void;

  public function updateFooter(): Void;

  public function goto(id: Int): Void;

  public function updateMenu(): Void;

  public function updateBad(): Void;

  public function square(cx1: Int, cy1: Int, cx2: Int, cy2: Int): Void;

  public function paint(cx: Int, cy: Int): Void;

  public function remove(cx: Int, cy: Int): Void;

  public function isEmpty(cx: Int, cy: Int): Bool;

  public function anyModified(): Bool;

  public function setModified(lid: Int): Void;

  public function updateCursor(): Void;

  public function showCaseCursor(): Void;

  public function hideCaseCursor(): Void;

  public function followPortal(pid: Int): Void;

  public function onLoadComplete(): Void;

  public function mouseDown(): Void;

  public function mouseUp(): Void;

  public function none(): Void;

  public function onNew(): Void;

  public function onSaveLevel(): Void;

  public function onLoadLevel(): Void;

  public function onLoadAll(): Void;

  public function onSaveAll(): Void;

  public function onCookieReset(): Void;

  public function onNextLevel(): Void;

  public function onPrevLevel(): Void;

  public function onNextLevelFast(): Void;

  public function onPrevLevelFast(): Void;

  public function onFirstLevel(): Void;

  public function onLastLevel(): Void;

  public function onPrevBg(): Void;

  public function onNextBg(): Void;

  public function onPrevTiles(): Void;

  public function onNextTiles(): Void;

  public function onPrevColumn(): Void;

  public function onNextColumn(): Void;

  public function onResetColumn(): Void;

  public function onQuit(): Void;

  public function onSelectTile(): Void;

  public function onSelectBad(): Void;

  public function onSelectField(): Void;

  public function onSelectStart(): Void;

  public function onSelectSpecial(): Void;

  public function onSelectScore(): Void;

  public function onPrevField(): Void;

  public function onNextField(): Void;

  public function onPrevBad(): Void;

  public function onNextBad(): Void;

  public function onCopy(): Void;

  public function onPaste(): Void;

  public function onStyleCopy(): Void;

  public function onStylePaste(): Void;

  public function onLoadAdv(): Void;

  public function onLoadMulti(): Void;

  public function onLoadTuto(): Void;

  public function onLoadShare(): Void;

  public function onLoadFjv(): Void;

  public function onLoadDev(): Void;

  public function onLoadTest(): Void;

  public function onLoadTime(): Void;

  public function onLoadMultiTime(): Void;

  public function onLoadSoccer(): Void;

  public function onLoadHof(): Void;

  public function onLoadDeep(): Void;

  public function onLoadHiko(): Void;

  public function onLoadAyame(): Void;

  public function onLoadHk(): Void;

  public function onBrowse(): Void;

  public function onTest(): Void;

  public function onScript(): Void;

  public function onItemBrowser(): Void;

  public function onQuestBrowser(): Void;

  public function onStartGame(): Void;

  public function onPanLeft(): Void;

  public function onPanRight(): Void;

  public function onPanUp(): Void;

  public function onPanDown(): Void;

  public function panMap(offsetX: Int, offsetY: Int): Void;

  public function clearColumn(x: Int): Void;

  override public function onSleep(): Void;

  override public function onWakeUp(n: Dynamic /*TODO*/, data: Dynamic /*TODO*/): Void;

  override public function getControls(): Void;

  override public function endMode(): Void;

  override public function destroy(): Void;

  override public function main(): Void;
}
