package hf.mode;

import hf.mode.Adventure;

extern class Fjv extends Adventure {
  public var fCookie: Dynamic /*TODO*/;

  public function new(m: Dynamic /*TODO*/, id: Dynamic /*TODO*/): Void;

  public function darknessManager(): Void;

  override public function initWorld(): Void;

  override public function initGame(): Void;

  override public function endMode(): Void;

  override public function usePortal(pid: Dynamic /*TODO*/, e: Dynamic /*TODO*/): Bool;

  override public function onEndOfSet(): Void;

  override public function onGameOver(): Void;

  override public function onLevelReady(): Void;

  override public function onPause(): Void;
}
