package hf.mode;

import etwin.flash.MovieClip;
import etwin.flash.SharedObject;
import hf.mode.Adventure;

extern class FjvEnd extends Adventure {
  public var fl_win: Bool;
  public var fCookie: SharedObject;
  public var scr: Dynamic /*TODO*/;
  public var bg: Dynamic /*TODO*/;
  public var timer: Float;
  public var fxList: Array<MovieClip>;
  public var fl_name: Bool;

  public function new(m: Dynamic /*TODO*/, win: Bool): Void;

  public function focus(): Void;

  override public function endMode(): Void;

  override public function main(): Void;
}
