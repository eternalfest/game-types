package hf.mode;

import etwin.flash.MovieClip;
import hf.Chrono;
import hf.entity.bomb.player.SoccerBall;
import hf.entity.Player;
import hf.GameManager;
import hf.mode.MultiCoop;

extern class Soccer extends MultiCoop {
  public static var BOOST_DISTANCE: Float;
  public static var MAX_BOOST: Float;
  public static var MATCH_DURATION: Int;
  public static var WARNING_TIME: Int;
  public static var BLINK_COLOR: Int;
  public static var BLINK_GLOWCOLOR: Int;
  public static var GOAL_OFFTIME: Float;
  public static var START_OFFTIME: Float;
  public static var END_TIMER: Float;

  public var fl_help: Bool;
  public var fl_party: Bool;
  public var fl_match: Bool;
  public var fl_end: Bool;
  public var chrono: Chrono;
  public var blinkTimer: Float;
  public var scores: Array<Int>;
  public var teams: Array<Array<Player>>;
  public var ball: Null<SoccerBall>;
  public var offPlayTimer: Null<Float>;
  public var winners: Int;
  public var finalTimer: Float;

  public function new(m: GameManager, id: Dynamic): Void;

  public function getTeam(p: Player): Int;

  public function getTeamPos(p: Player): Int;

  public function getTeamScore(tid: Int): Int;

  public function goal(tid: Int): Void;

  public function endMatch(): Void;

  public function showResults(): Void;

  public function onEndMode(): Void;

  public function insertBall(): Void;

  public function darknessManager(): Void;

  override public function addLevelItems(): Void;

  override public function endMode(): Void;

  override public function forcedGoto(id: Int): Void;

  override public function getDebugControls(): Void;

  override public function initGame(): Void;

  override public function initInterface(): Void;

  override public function initPlayer(p: Player): Void;

  override public function initWorld(): Void;

  override public function lock(): Void;

  override public function main(): Void;

  override public function onGameOver(): Void;

  override public function onHurryUp(): Null<MovieClip>;

  override public function onLevelReady(): Void;

  override public function onPause(): Void;

  override public function onUnpause(): Void;

  override public function unlock(): Void;
}
