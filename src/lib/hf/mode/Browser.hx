package hf.mode;

import hf.gui.Container;
import hf.levels.SetManager;
import hf.Mode;

extern class Browser extends Mode {
  public var current: Int;
  public var initialId: Dynamic /*TODO*/;
  public var dimensionId: Dynamic /*TODO*/;
  public var fl_modified: Array<Bool>;
  public var fl_buildCache: Bool;
  public var world: SetManager;
  public var navC: Container;
  public var menuC: Container;
  public var draw: Dynamic /*TODO*/;
  public var footer: Dynamic /*TODO*/;
  public var first: Int;
  public var fields: Dynamic /*TODO*/;
  public var buffer: Dynamic /*TODO*/;
  public var cacheId: Dynamic /*TODO*/;
  public var views: Dynamic /*TODO*/;

  public static var THUMBS: Int;
  public static var LINE: Int;
  public static var PAGE: Int;
  public static var SCALE: Float;

  public function new(m: Dynamic /*TODO*/, w: SetManager, did: Dynamic /*TODO*/, fl_mod: Array<Bool>): Void;

  public function refresh(): Void;

  public function updateBoxes(): Void;

  public function highlight(view: Dynamic /*TODO*/, thickness: Float, color: Dynamic /*TODO*/): Void;

  public function updatePage(): Void;

  public function swapLevels(from: Dynamic /*TODO*/, to: Dynamic /*TODO*/): Bool;

  public function onPrevious(): Void;

  public function onNext(): Void;

  public function onPreviousLine(): Void;

  public function onNextLine(): Void;

  public function onPreviousPage(): Void;

  public function onNextPage(): Void;

  public function onFirst(): Void;

  public function onLast(): Void;

  public function onValidate(): Void;

  public function onCancel(): Void;

  public function onMoveAfter(): Void;

  public function onMoveBefore(): Void;

  public function onDelete(): Void;

  public function onInsert(): Void;

  public function onClear(): Void;

  public function onCut(): Void;

  public function onCopy(): Void;

  public function onPaste(): Void;

  public function onSwapBuffer(): Void;

  public function onInsertAfter(): Void;

  public function onBuildCache(): Void;

  override public function init(): Void;

  override public function endMode(): Void;

  override public function destroy(): Void;

  override public function main(): Void;
}
