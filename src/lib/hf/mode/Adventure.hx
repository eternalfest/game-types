package hf.mode;

import etwin.flash.MovieClip;
import hf.entity.Bad;
import hf.GameManager;
import hf.mode.GameMode;

extern class Adventure extends GameMode {
  public static var BUCKET_X: Int;
  public static var BUCKET_Y: Int;

  public var firstLevel: Int;
  public var perfectOrder: Array<Int>;
  public var perfectCount: Int;
  public var trackPos: Null<Float>;

  public function new(m: GameManager, ?id: Int): Void;

  override public function addLevelItems(): Void;

  override public function attachBad(id: Int, x: Float, y: Float): Bad;

  override public function destroy(): Void;

  override public function endMode(): Void;

  override public function getDebugControls(): Void;

  override public function goto(id: Int): Void;

  override public function initGame(): Void;

  override public function init(): Void;

  override public function initWorld(): Void;

  override public function isBossLevel(id: Int): Bool;

  override public function nextLevel(): Void;

  override public function onEndOfSet(): Void;

  override public function onExplode(x: Float, y: Float, radius: Float): Void;

  override public function onGameOver(): Void;

  override public function onHurryUp(): Null<MovieClip>;

  override public function onKillBad(b: Bad): Void;

  override public function onLevelClear(): Void;

  override public function onLevelReady(): Void;

  override public function resetHurry(): Void;

  override public function saveScore(): Void;

  override public function startLevel(): Void;
}
