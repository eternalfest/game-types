package hf.mode;

import etwin.flash.MovieClip;
import hf.GameManager;
import hf.mode.GameMode;

extern class Tutorial extends GameMode {
  public function new(m: GameManager): Void;

  public function darknessManager(): Void;

  override public function endMode(): Void;

  override public function initGame(): Void;

  override public function init(): Void;

  override public function initInterface(): Void;

  override public function initWorld(): Void;

  override public function onEndOfSet(): Void;

  override public function onGameOver(): Void;

  override public function onHurryUp(): Null<MovieClip>;

  override public function onLevelReady(): Void;

  override public function onPause(): Void;
}
