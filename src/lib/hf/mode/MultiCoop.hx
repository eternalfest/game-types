package hf.mode;

import hf.entity.Player;
import hf.GameManager;
import hf.mode.Adventure;

extern class MultiCoop extends Adventure {
  public function new(m: GameManager, id: Int): Void;

  override public function initGame(): Void;

  override public function initPlayer(p: Player): Void;

  override public function onPause(): Void;

  override public function saveScore(): Void;
}
