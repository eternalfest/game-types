package hf.mode;

import hf.mode.Adventure;

extern class AdventureTest extends Adventure {
  public var testData: Dynamic /*TODO*/;

  public function new(m: Dynamic /*TODO*/, l: Dynamic /*TODO*/): Void;

  override public function startLevel(): Void;

  override public function initWorld(): Void;

  override public function nextLevel(): Void;

  override public function endMode(): Void;

  override public function onEndOfSet(): Void;

  override public function onGameOver(): Void;

  override public function main(): Void;
}
