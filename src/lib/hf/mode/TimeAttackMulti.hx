package hf.mode;

import hf.entity.Player;
import hf.GameManager;
import hf.mode.TimeAttack;

extern class TimeAttackMulti extends TimeAttack {
  public function new(m: GameManager, id: Int): Void;

  override public function initGame(): Void;

  override public function initPlayer(p: Player): Void;

  override public function initRunId(): Void;

  override public function initWorld(): Void;
}
