package hf.mode;

import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.GameManager;
import hf.mode.Adventure;

extern class TimeAttack extends Adventure {
  public static var INFINITY: Int;

  public var runId: Null<Int>;
  public var suspendTimer: Null<Int>;
  public var times: Array<Int>;
  public var frameTimer: Int;
  public var fl_alerts: Array<Bool>;
  public var prevFrameTimer: Int;
  public var gameTimer: Int;
  public var starter: Float;
  public var mcStarter: DynamicMovieClip /* TODO */;
  public var last: Int;
  public var fl_chronoStop: Bool;

  public function new(m: GameManager, id: Int): Void;

  public function initRunId(): Void;

  public function initializePlayers(): Void;

  public function getChrono(): Int;

  public function display(str: String): Void;

  public function displayLastTime(): Void;

  public function formatTime(t: Int): String;

  public function resetChrono(): Void;

  public function startChrono(): Void;

  public function stopChrono(): Void;

  public function timeShift(n: Float): Void;

  override public function addLevelItems(): Void;

  override public function goto(id: Int): Void;

  override public function initGame(): Void;

  override public function initWorld(): Void;

  override public function lock(): Void;

  override public function main(): Void;

  override public function onEndOfSet(): Void;

  override public function onGameOver(): Void;

  override public function onLevelClear(): Void;

  override public function onLevelReady(): Void;

  override public function onResurrect(): Void;

  override public function pickUpScore(id: Int, sid: Int): Int;

  override public function saveScore(): Void;

  override public function switchDimensionById(id: Int, lid: Int, pid: Int): Void;

  override public function unlock(): Void;
}
