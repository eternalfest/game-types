package hf.mode;

import hf.Chrono;
import hf.entity.Player;
import hf.GameManager;
import hf.mode.Adventure;

extern class BossRush extends Adventure {
  public var levelTimer: Int;
  public var levelAvg: Float;
  public var lastLevelTimer: Null<Int>;
  public var chrono: Chrono;
  public var levelCount: Dynamic /*TODO*/;

  public function new(m: GameManager, id: Int): Void;

  public function upgradePlayer(p: Player): Void;

  public function registerLevelTime(): Void;

  public function displayTime(t: Int): Void;

  override public function goto(id: Int): Void;

  override public function initPlayer(p: Player): Void;

  override public function lock(): Void;

  override public function main(): Void;

  override public function onPause(): Void;

  override public function onResurrect(): Void;

  override public function startLevel(): Void;

  override public function switchDimensionById(did: Int, lid: Int, pid: Int): Void;

  override public function unlock(): Void;
}
