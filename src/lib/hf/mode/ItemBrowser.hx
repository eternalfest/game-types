package hf.mode;

import hf.Mode;

extern class ItemBrowser extends Mode {
  public var page: Int;
  public var mcList: Array<Dynamic /*TODO*/>;
  public var fl_score: Bool;
  public var fl_names: Bool;
  public var header: Dynamic /*TODO*/;
  public var footer: Dynamic /*TODO*/;
  public var ffilter: Dynamic /*TODO*/;
  public var fullRandList: Array<Dynamic /*TODO*/>;
  public var max_pages: Int;
  public var klock: Dynamic /*TODO*/;

  public static var ITEM_WIDTH: Float;
  public static var ITEM_HEIGHT: Float;
  public static var PAGE_LENGTH: Int;

  public function new(m: Dynamic /*TODO*/): Void;

  public function getRandColor(proba: Int): Int;

  public function refresh(): Void;

  public function onOver(id: Int): Void;

  override public function endMode(): Void;

  override public function main(): Void;
}
