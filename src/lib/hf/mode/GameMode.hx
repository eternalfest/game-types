package hf.mode;

import etwin.flash.Color;
import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.Chrono;
import hf.Data.ItemInfo;
import hf.entity.Bad;
import hf.entity.Player;
import hf.Entity;
import hf.fx.Darkness;
import hf.FxManager;
import hf.GameManager;
import hf.interf.ItemName;
import hf.levels.GameMechanics;
import hf.levels.PortalLink;
import hf.Mode;
import hf.RandomManager;
import hf.StatsManager;

typedef ExtraHole = {
  var x: Float;
  var y: Float;
  var d: Float;
  var mc: MovieClip;
}

typedef MapEvent = {
  var lid: Int;
  var eid: Int;
  var misc: String;
}

typedef PlayerSpawn = {
  var x: Float;
  var y: Float;
  var fl_unstable: Bool;
}

typedef PortalMc = {
  var x: Float;
  var y: Float;
  var cpt: Float;
  var mc: MovieClip;
}

typedef Unreg = {
  var type: Int;
  var ent: Entity;
}

extern class GameMode extends Mode {
  public static var WATER_COLOR: Int;

  public var duration: Float;
  public var lagCpt: Float;
  public var fl_static: Bool;
  public var fl_bullet: Bool;
  public var fl_disguise: Bool;
  public var fl_map: Bool;
  public var fl_mirror: Bool;
  public var fl_nightmare: Bool;
  public var fl_ninja: Bool;
  public var fl_bombExpert: Bool;
  public var fl_bombControl: Bool;
  public var savedScores: Array<Int>;
  public var fxMan: FxManager;
  public var statsMan: StatsManager;
  public var randMan: RandomManager;
  public var speedFactor: Float;
  public var diffFactor: Float;
  public var comboList: Array<Int>;
  public var killList: Array<Entity>;
  public var unregList: Array<Unreg>;
  public var lists: Array<Array<Entity>>;
  public var globalActives: Array<Bool>;
  public var endLevelStack: Array<Void -> Void>;
  public var portalMcList: Array<PortalMc>;
  public var extraHoles: Array<ExtraHole>;
  public var dfactor: Float;
  public var forcedDarkness: Null<Float>;
  public var targetDark: Null<Float>;
  public var darknessMC: Null<Darkness>;
  public var aquaTimer: Float;
  public var bulletTimer: Float;
  public var fl_flipX: Bool;
  public var fl_flipY: Bool;
  public var fl_clear: Bool;
  public var fl_gameOver: Bool;
  public var huTimer: Float;
  public var huState: Int;
  public var tipId: Int;
  public var specialPicks: Array<Int>;
  public var scorePicks: Array<Int>;
  public var currentDim: Int;
  public var dimensions: Array<GameMechanics>;
  public var latePlayers: Array<Player>;
  public var mapIcons: Array<MovieClip>;
  public var mapEvents: Array<MapEvent>;
  public var mapTravels: Array<MapEvent>;
  public var gameChrono: Chrono;
  public var endModeTimer: Float;
  public var worldKeys: Array<Bool>;
  public var gi: Dynamic; // TODO: GameInterface
  public var world: GameMechanics;
  public var shakeTimer: Float;
  public var shakeTotal: Float;
  public var shakePower: Float;
  public var fl_wind: Null<Bool>;
  public var windSpeed: Float;
  public var windTimer: Float;
  public var badCount: Int;
  public var keyLock: Int;
  public var fl_badsCallback: Null<Bool>;
  public var fakeLevelId: Int;
  public var gFriction: Float;
  public var sFriction: Float;
  public var dvars: Hash;
  public var color: Null<Color>;
  public var colorHex: Null<Int>;
  public var portalId: Int;
  public var nextLink: PortalLink;
  public var fl_rightPortal: Null<Bool>;
  public var popMC: DynamicMovieClip /* TODO */;
  public var pointerMC: MovieClip;
  public var radiusMC: MovieClip;
  public var itemNameMC: Null<ItemName>;
  public var friendsLimit: Null<Int>;
  public var fl_pause: Null<Bool>;
  public var pauseMC: Null<DynamicMovieClip /* TODO */>;
  public var mapMC: DynamicMovieClip /* TODO */;
  public var fl_ice: Null<Bool>;
  public var fl_aqua: Null<Bool>;

  /**
   * Will do nothing except in `hf.mode.Adventure`, but `hf.levels.ScriptEngine` uses it on
   * `hf.mode.GameMode`.
   *
   * TODO: Move to `hf.mode.Adventure`?
   */
  public var fl_warpStart: Null<Bool>;
  // Used by SpecialManager
  public var perfectItemCpt: Null<Int>;

  public function new(m: GameManager): Void;

  public function initWorld(): Void;

  public function initGame(): Void;

  public function initInterface(): Void;

  public function initLevel(): Void;

  public function initPlayer(p: Player): Void;

  public function shake(duration: Null<Float>, power: Null<Float>): Void;

  public function wind(speed: Null<Float>, duration: Null<Float>): Void;

  public function flipX(fl: Bool): Void;

  public function flipY(fl: Bool): Void;

  public function resetHurry(): Void;

  public function checkLevelClear(): Bool;

  public function printDebugInfos(): Void;

  public function printBetaInfos(): Void;

  public function addLevelItems(): Void;

  public function startLevel(): Void;

  public function nextLevel(): Void;

  public function forcedGoto(id: Int): Void;

  public function clearLevel(): Void;

  public function goto(id: Int): Void;

  public function countCombo(id: Int): Int;

  public function canAddItem(): Bool;

  public function bulletTime(d: Float): Void;

  public function updateGroundFrictions(): Void;

  public function pickUpSpecial(id: Int): Int;

  public function pickUpScore(id: Int, subId: Int): Int;

  public function getPicks(): String;

  public function getPicks2(): Array<Int>;

  public function isBossLevel(id: Int): Bool;

  public function flipCoordReal(x: Float): Float;

  // TODO: Generic
  public function flipCoordCase(cx: Float): Float;

  public function registerMapEvent(eid: Int, misc: Dynamic /*TODO*/): Void;

  public function getMapY(lid: Int): Float;

  public function setDynamicVar(name: String, value: Dynamic /*TODO*/): Void;

  public function getDynamicVar(name: String): Dynamic /*TODO*/;

  public function getDynamicInt(name: String): Int;

  public function clearDynamicVars(): Void;

  public function saveScore(): Void;

  public function registerScore(pid: Int, score: Int): Void;

  public function setColorHex(a: Float, col: Int): Void;

  public function resetCol(): Void;

  public function giveKey(id: Int): Void;

  public function hasKey(id: Int): Bool;

  public function switchDimensionById(id: Int, lid: Int, pid: Int): Void;

  public function addWorld(name: String): GameMechanics;

  public function getPortalEntrance(pid: Int): PlayerSpawn;

  public function usePortal(pid: Int, e: Entity): Bool;

  public function openPortal(cx: Float, cy: Float, pid: Int): Bool;

  public function getListId(type: Int): Int;

  public function getList(type: Int): Array<Entity>;

  public function getListAt(type: Int, cx: Int, cy: Int): Array<Entity>;

  public function countList(type: Int): Int;

  public function getBadList(): Array<Bad>;

  public function getBadClearList(): Array<Bad>;

  public function getPlayerList(): Array<Player>;

  public function getListCopy(type: Int): Array<Entity>;

  public function getClose(type: Int, x: Float, y: Float, radius: Float, fl_onGround: Bool): Array<Entity>;

  public function getOne(type: Int): Null<Entity>;

  public function getAnotherOne(type: Int, e: Entity): Null<Entity>;

  public function addToList(type: Int, e: Entity): Void;

  public function removeFromList(type: Int, e: Entity): Void;

  public function destroyList(type: Int): Void;

  public function destroySome(type: Int, n: Int): Void;

  public function cleanKills(): Void;

  public function exitGame(): Void;

  public function insertPlayer(cx: Float, cy: Float): Player;

  public function attachBad(id: Int, x: Float, y: Float): Bad;

  public function attachPop(msg: String, fl_tuto: Bool): Void;

  public function attachPointer(cx: Float, cy: Float, ocx: Float, ocy: Float): Void;

  public function attachRadius(x: Float, y: Float, r: Float): Void;

  public function attachItemName(family: Array<Array<ItemInfo>>, id: Int): Void;

  public function killPop(): Void;

  public function killPointer(): Void;

  public function killRadius(): Void;

  public function killItemName(): Void;

  public function killPortals(): Void;

  public function attachMapIcon(eid: Int, lid: Int, txt: String, offset: Float, offsetTotal: Int): Void;

  public function callEvilOne(baseAnger: Int): Void;

  public function onLevelReady(): Void;

  public function onBadsReady(): Void;

  public function onRestore(): Void;

  public function updateEntitiesWorld(): Void;

  public function onLevelClear(): Void;

  public function onHurryUp(): Null<MovieClip>;

  public function onGameOver(): Void;

  public function onKillBad(b: Bad): Void;

  public function onPause(): Void;

  public function onMap(): Void;

  public function onUnpause(): Void;

  public function onResurrect(): Void;

  public function onExplode(x: Float, y: Float, radius: Float): Void;

  public function onEndOfSet(): Void;

  public function updateDarkness(): Void;

  public function addHole(x: Float, y: Float, diameter: Float): Void;

  public function detachExtraHoles(): Void;

  public function clearExtraHoles(): Void;

  public function holeUpdate(): Void;

  override public function destroy(): Void;

  override public function getControls(): Void;

  override public function getDebugControls(): Void;

  override public function lock(): Void;

  override public function main(): Void;

  override public function unlock(): Void;
}
