package hf;

import hf.BitCodec;
import hf.Hash;

extern class PersistCodec {
  public var obfu_mode: Bool;
  public var crc: Bool;
  public var fast: Null<Bool>;
  // An older version of this class may be found on the internet, it calls this field `bc`.
  public var pou: Null<BitCodec>;
  public var fields: Null<Hash>;
  public var nfields: Null<Int>;
  public var next_field_bits: Null<Int>;
  public var nfields_bits: Null<Int>;
  public var cache: Null<Array<Dynamic>>;
  public var fieldtbl: Null<Array<Dynamic>>;
  public var result: Null<Dynamic>;

  public function new(): Void;

  public function encode_array(a: Array<Dynamic>): Void;

  public function decode_array(): Array<Dynamic>;

  public function decode_array_item(a: Dynamic): Bool;

  public function decode_array_fast(): Array<Dynamic>;

  public function encode_string(s: String): Void;

  public function decode_string(): String;

  public function encode_object(o: Dynamic): Void;

  public function encode_object_field(k: String, d: Dynamic): Void;

  public function decode_object_fast(): Dynamic;

  public function decode_object(): Dynamic;

  public function decode_object_field(o: Dynamic): Bool;

  public function encode_int(o: Int): Void;

  public function decode_int(): Int;

  public function encode_float(o: Float): Void;

  public function decode_float(): Float;

  public function do_encode(o: Dynamic): Bool;

  public function do_decode(): Dynamic;

  public function encodeInit(o: Dynamic): Void;

  public function encodeLoop(): Bool;

  public function encodeEnd(): String;

  public function encode(o: Dynamic): String;

  public function progress(): Float;

  public function decodeInit(data: String): Void;

  public function decodeLoop(): Bool;

  public function decodeEnd(): Dynamic;

  public function decode(data: String): Dynamic;
}
